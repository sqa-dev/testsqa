﻿SET ANSI_NULLS, QUOTED_IDENTIFIER ON
GO
CREATE or alter procedure dbo.Validar_estructura_ASCII
    @I_nombre_archivo   varchar(50)='',
    @I_tipo_error       tinyint=0,-- 0= que valide uno por Uno 1= que muestre al final los errores
    @I_idtabla          int=0,
    @I_Tipo_archivo     tinyint=0,
    @I_cant_titulo      smallint=0,
    @I_separador        char(1)='|',
    @I_nro_linea        int=0,-- el nro de la linea
    @I_linea            varchar(max)='',
    @O_errores          varchar(max)='' output,
    @O_Desc_errores     varchar(max)='' output,
    @O_linea_error_formato varchar(max)='' output,
    @O_nro_error        smallint=0      output,
    @O_estructura       tinyint =0      output,
    @O_error_msg        varchar(300)='' output
WITH ENCRYPTION
as
/**************************************************************************************************/
/* DESCRIPCIÓN   : Valida los archivos a importar                                                 */
/* OBSERVACIONES : RD-8837 IV, indentacion y cambio de ltrm/str por cast                          */
/* REV. CALIDAD  : OK                                                                             */
/**************************************************************************************************/
set nocount on

declare
    @F_campo            varchar(400),
    @F_linea            varchar(max),
    @F_tipo_variable    tinyint,
    @F_tamanio_variable int,
    @F_opcional         tinyint,
    @F_nro_campo        int,
    @F_nombre_titulo    varchar(40),
    @F_tipo_titulo      tinyint,
    @F_nro_linea        int,      -- es el nro de linea del titulo
    @z_existe_linea     int,
    @z_nro_linea        int,
    @F_error            int,
    @F_rowcount         int,
    @F_existe_separador smallint,
    @F_salir            tinyint,
    @F_formato          int,
    @F_Error_Exec       int,
    @F_fecha            smalldatetime,
    @F_posiscion        int,
    @F_cantidad_campo   decimal(16),
    @F_error_msg        varchar(300),
    @F_error_auto       VARCHAR(max),
    @F_error_auto_aux   VARCHAR(max),
    @F_str_cod_error    Varchar(3) = '',
    @CRLF               varchar(10) = Char(13) + Char(10)--salto de linea

---------------------------------------------------------------------------------------------------
--           VALIDAR EL CLIENTE
---------------------------------------------------------------------------------------------------
set @F_error_auto_aux = ''
set @F_error_auto = ''

If isnull(@I_idtabla, -1) < 0
begin
    set @O_error_msg = 'Necesito el Código de la tabla= ' + ltrim(STR(@I_idtabla) )

    If @I_tipo_error = 1
    Begin
        --set @O_errores      = dbo.Fn_Adicionar_Error_PLA(@O_errores,'E1',',')
        --set @O_Desc_errores = dbo.Fn_Adicionar_Error_PLA(@O_Desc_errores,'Error E1: '+ @O_error_msg,CHAR(13)+space(20))
        set @F_str_cod_error  = 'E1'
        set @O_errores        = dbo.Fn_Adicionar_Error_PLA(@O_errores,@F_str_cod_error,',')
        set @O_Desc_errores   = dbo.Fn_Adicionar_Error_PLA(@O_Desc_errores,'Error '+@F_str_cod_error+': '+ @O_error_msg,@CRLF)
        set @F_error_auto_aux = ''
        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,ltrim(@I_nombre_archivo),'|')
        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,' ','|')
        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,@F_str_cod_error,'|')
        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,ltrim(@O_error_msg),'|')
        set @F_error_auto     = dbo.Fn_Adicionar_Error_PLA(@F_error_auto, @F_error_auto_aux,@CRLF)
    End
    else
        GOTO linea_error
end

If @I_Tipo_archivo = 2 -- si es excel
begin
    set @I_separador = '|'
end

declare
    @temp as table(
        campo            int,--identity(1,1),
        tipo_variable    tinyint,
        tamanio_variable int,
        posicion         int,
        opcional         tinyint,
        nombre_titulo    varchar(40),
        tipo_titulo      tinyint,
        nro_linea        int,
        estado           tinyint
    )

delete from @temp
Insert into @temp
select
    b.secuencia,
    b.tipo_variable,
    b.tamanio_variable,
    b.posicion,
    b.opcional,
    --campos para excel
    b.nombre_titulo,
    b.tipo_titulo,
    b.nro_linea,     -- es el nro de linea del titulo
    0 --
from pam_tipoTablaDetalle as b
where b.idTabla = @I_idtabla
    and b.fecha_proceso_hasta ='01-01-2050'
    and b.indicador ='A'
order by b.tipo_titulo, b.nro_linea, b.secuencia  --Secuencia de los campos

Select
    @F_linea = rtrim(ltrim(@I_linea)),
    @F_nro_campo        = 1,
    @z_existe_linea     = 0,
    @F_existe_separador = 0,
    @F_salir            = 0,
    @F_cantidad_campo   = 0

While (len(ltrim(rtrim(isnull(@F_linea, '')))) > 0 Or @F_existe_separador = 1)
    and @F_salir =0
Begin
        -- Busca el Formato que va tomar encuenta  si es para la cabecera o para el detalle
    If @I_cant_titulo > 1
        set @z_nro_linea =  dbo.Fn_pam_Obtener_titulo_pam(@I_nro_linea,@I_idtabla)
    Else
    begin
        select top 1 @z_nro_linea = nro_linea
        from pam_tipoTablaDetalle as b
        where b.idTabla = @I_idtabla
            and b.fecha_proceso_hasta ='01-01-2050'
            and b.indicador ='A'
        GROUP by nro_linea, tipo_titulo
        order by b.nro_linea
    end

    Select @F_cantidad_campo = COUNT(1)
    from @temp

    select
        @F_tipo_variable    = tipo_variable,
        @F_tamanio_variable = tamanio_variable,
        @F_posiscion        = posicion,
        @F_opcional         = opcional,          -- 0=No exigible  1=exigible
        @F_nombre_titulo    = nombre_titulo,
        @F_tipo_titulo      = tipo_titulo,
        @F_nro_linea        = nro_linea      -- es el nro de linea del titulo
    from @temp
    Where campo = @F_nro_campo
        and nro_linea = @z_nro_linea

    select @F_rowcount = @@ROWCOUNT, @F_error= @@ERROR

    if @F_error <> 0 and @F_rowcount > 1
    begin
        set @O_error_msg = 'ERROR: al leer @temp en la fila= ' +ltrim(str(@I_nro_linea))
        GOTO linea_error
    end

    If @F_rowcount  = 0
    begin
        If (@F_posiscion =0)--Cuando tiene separador
        Begin
            set @O_error_msg = 'En la fila= ' + cast(@I_nro_linea as varchar(10)) + ' se tienen demasiado parámetros         ' + @F_linea--+cast(@z_nro_linea as varchar(10))

            If @I_tipo_error = 1
            Begin
                --set @O_errores      = dbo.Fn_Adicionar_Error_PLA(@O_errores,'E2',',')
                --set @O_Desc_errores = dbo.Fn_Adicionar_Error_PLA(@O_Desc_errores ,'Error E2: '+ @O_error_msg,CHAR(13)+space(20))
                set @F_str_cod_error  ='E2'
                set @O_errores        = dbo.Fn_Adicionar_Error_PLA(@O_errores,@F_str_cod_error,',')
                set @O_Desc_errores   = dbo.Fn_Adicionar_Error_PLA(@O_Desc_errores,'Error '+@F_str_cod_error+': '+ @O_error_msg,@CRLF)
                set @F_error_auto_aux = ''
                set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,ltrim(@I_nombre_archivo),'|')
                set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,' ','|')
                set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,@F_str_cod_error,'|')
                set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,ltrim(@O_error_msg),'|')
                set @F_error_auto   = dbo.Fn_Adicionar_Error_PLA(@F_error_auto, @F_error_auto_aux,@CRLF)
                set @F_salir  = 1
            End
            else
                GOTO linea_error
        end
        Else
            set @F_salir = 2-- para que se sal
    end-- FIN @F_rowcount  = 0

    --PARA OBTENER EL CAMPO
    If @F_salir in(0, 1)
    begin
        If CHARINDEX(@I_separador,@F_linea)> 0--si tiene separadores
            and len(ltrim(@I_separador))>0        --cuando tiene separador
        begin
            set @F_campo = SUBSTRING(@F_linea,1,(CHARINDEX(@I_separador,@F_linea,1))-1)
            set @F_linea = SUBSTRING(@F_linea,case when CHARINDEX(@I_separador,@F_linea)>0 then (CHARINDEX(@I_separador,@F_linea,1)+1)
                                                    else LEN(@F_linea)+1  end,LEN(@F_linea))
            set @F_existe_separador = 1
        end
        else
        begin--Adicionado Por Gabriel
            If len(ltrim(@I_separador))=0     --cuando NO tiene separador
                set @F_campo = SUBSTRING(@F_linea,@F_posiscion,@F_tamanio_variable)
            else
            begin
                set @F_campo = SUBSTRING(@F_linea,1,LEN(@F_linea))
                set @F_linea = ''
                set @F_existe_separador = 0
                end
            end
            --set @F_nro_campo += 1  --Incrementar
        End
        --
        --select 'campo',@F_campo,@F_linea

        If @F_salir = 0 --Que continue validando los campos
        Begin
            If @z_nro_linea = @I_nro_linea   -- si la linea es el titulo y ademas tiene titulo
                and len(ltrim(rtrim(@F_nombre_titulo)))>0
            begin
                If @F_campo <> @F_nombre_titulo
                Begin
                    set @O_error_msg = 'El Titulo '+ltrim(@F_nombre_titulo)+' es diferente al del excel= '+ltrim(@F_campo)+' de la Fila =' + cast(@I_nro_linea as varchar(10)) +
                                    ' en la columna= ' + cast(@F_nro_campo as varchar(10))
                If @I_tipo_error = 1
                Begin
                    --set @O_errores      = dbo.Fn_Adicionar_Error_PLA(@O_errores,'E3',',')
                    --set @O_Desc_errores = dbo.Fn_Adicionar_Error_PLA(@O_Desc_errores,'Error E3: '+@O_error_msg,CHAR(13)+space(20))
                    set @F_str_cod_error  ='E3'
                    set @O_errores        = dbo.Fn_Adicionar_Error_PLA(@O_errores,@F_str_cod_error,',')
                    set @O_Desc_errores   = dbo.Fn_Adicionar_Error_PLA(@O_Desc_errores,'Error '+@F_str_cod_error+': '+ @O_error_msg,@CRLF)
                    set @F_error_auto_aux = ''
                    set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,ltrim(@I_nombre_archivo),'|')
                    set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,' ','|')
                    set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                    set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                    set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                    set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,@F_str_cod_error,'|')
                    set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,ltrim(@O_error_msg),'|')
                    set @F_error_auto     = dbo.Fn_Adicionar_Error_PLA(@F_error_auto, @F_error_auto_aux,@CRLF)
                End
                else
                    GOTO linea_error
            End
        end
        else
        Begin
            If @F_tipo_variable in (1, 2)--Numérico o decimal
            begin
                If ISNUMERIC(@F_campo) = 0 AND LEN(LTRIM(rtrim(@F_campo)))>0
                Begin
                    set @O_error_msg = 'El Campo ' + cast(@F_nro_campo as varchar(10)) + ' de la Fila =' + cast(@I_nro_linea as varchar(10)) +
                                        ' Debe ser Numérico '
                    If @I_tipo_error = 1
                    Begin
                        --set @O_errores      = dbo.Fn_Adicionar_Error_PLA(@O_errores,'E4',',')
                        --set @O_Desc_errores = dbo.Fn_Adicionar_Error_PLA(@O_Desc_errores,'Error E4: '+ @O_error_msg,CHAR(13)+space(20))
                        set @F_str_cod_error  = 'E4'
                        set @O_errores        = dbo.Fn_Adicionar_Error_PLA(@O_errores,@F_str_cod_error,',')
                        set @O_Desc_errores   = dbo.Fn_Adicionar_Error_PLA(@O_Desc_errores,'Error '+@F_str_cod_error+': '+ @O_error_msg,@CRLF)
                        set @F_error_auto_aux = ''
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,ltrim(@I_nombre_archivo),'|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,' ','|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,@F_str_cod_error,'|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,ltrim(@O_error_msg),'|')
                        set @F_error_auto     = dbo.Fn_Adicionar_Error_PLA(@F_error_auto, @F_error_auto_aux,@CRLF)
                    End
                    else
                        GOTO linea_error
                End

                If @F_opcional =2   --si es obligatorio
                    and LEN(LTRIM(rtrim(@F_campo)))=0
                Begin
                    set @O_error_msg = 'El Campo ' + cast(@F_nro_campo as varchar(10)) + ' de la Fila =' + cast(@I_nro_linea as varchar(10)) +
                                        ' Debe tener valor Numérico '
                    If @I_tipo_error = 1
                    Begin
                        --set @O_errores      = dbo.Fn_Adicionar_Error_PLA(@O_errores,'E5',',')
                        --set @O_Desc_errores = dbo.Fn_Adicionar_Error_PLA(@O_Desc_errores,'Error E5: '+ @O_error_msg,CHAR(13)+space(20))
                        set @F_str_cod_error  ='E5'
                        set @O_errores        = dbo.Fn_Adicionar_Error_PLA(@O_errores,@F_str_cod_error,',')
                        set @O_Desc_errores   = dbo.Fn_Adicionar_Error_PLA(@O_Desc_errores,'Error '+@F_str_cod_error+': '+ @O_error_msg,@CRLF)
                        set @F_error_auto_aux = ''
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,ltrim(@I_nombre_archivo),'|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,' ','|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,@F_str_cod_error,'|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,ltrim(@O_error_msg),'|')
                        set @F_error_auto     = dbo.Fn_Adicionar_Error_PLA(@F_error_auto, @F_error_auto_aux,@CRLF)
                    End
                    else
                        GOTO linea_error
                End
            end
            If @F_tipo_variable = 3--String
            begin
                If @F_opcional =2   --si es obligatorio
                    and LEN(LTRIM(rtrim(@F_campo)))=0
                Begin
                    set @O_error_msg = 'El Campo ' + cast(@F_nro_campo as varchar(10)) + ' de la Fila =' + cast(@I_nro_linea as varchar(10)) +
                                    ' Debe Tener Datos'
                    If @I_tipo_error = 1
                    Begin
                        --set @O_errores      = dbo.Fn_Adicionar_Error_PLA(@O_errores,'E6',',')
                        --set @O_Desc_errores = dbo.Fn_Adicionar_Error_PLA(@O_Desc_errores,'Error E6: '+ @O_error_msg,CHAR(13)+space(20))
                        set @F_str_cod_error  ='E6'
                        set @O_errores        = dbo.Fn_Adicionar_Error_PLA(@O_errores,@F_str_cod_error,',')
                        set @O_Desc_errores   = dbo.Fn_Adicionar_Error_PLA(@O_Desc_errores,'Error '+@F_str_cod_error+': '+ @O_error_msg,@CRLF)
                        set @F_error_auto_aux = ''
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,ltrim(@I_nombre_archivo),'|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,' ','|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,@F_str_cod_error,'|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,ltrim(@O_error_msg),'|')
                        set @F_error_auto     = dbo.Fn_Adicionar_Error_PLA(@F_error_auto, @F_error_auto_aux,@CRLF)
                    End
                    else
                        GOTO linea_error
                End

                If LEN(LTRIM(rtrim(@F_campo))) > @F_tamanio_variable
                Begin
                    set @O_error_msg = 'El Campo ' + cast(@F_nro_campo as varchar(10)) + ' de la Fila =' + cast(@I_nro_linea as varchar(10)) +
                                        'La longitud ha sobrepasdo al del Parametro'+ @F_campo--+' '+
                    If @I_tipo_error = 1
                    Begin
                        --set @O_errores      = dbo.Fn_Adicionar_Error_PLA(@O_errores,'E7',',')
                        --set @O_Desc_errores = dbo.Fn_Adicionar_Error_PLA(@O_Desc_errores,'Error E7: '+ @O_error_msg,CHAR(13)+space(20))
                        set @F_str_cod_error  = 'E7'
                        set @O_errores        = dbo.Fn_Adicionar_Error_PLA(@O_errores,@F_str_cod_error,',')
                        set @O_Desc_errores   = dbo.Fn_Adicionar_Error_PLA(@O_Desc_errores,'Error '+@F_str_cod_error+': '+ @O_error_msg,@CRLF)
                        set @F_error_auto_aux = ''
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,ltrim(@I_nombre_archivo),'|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,' ','|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,@F_str_cod_error,'|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,ltrim(@O_error_msg),'|')
                        set @F_error_auto   = dbo.Fn_Adicionar_Error_PLA(@F_error_auto, @F_error_auto_aux,@CRLF)
                    End
                    else
                        GOTO linea_error
                End
            end

            If @F_tipo_variable = 4--Fecha
            begin
                If ISDATE(@F_campo)= 0
                    and LEN(LTRIM(rtrim(@F_campo))) > 0
                Begin
                    exec @F_Error_Exec = dbo.proc_cctconvertfechayyyymmdd_a_ddmmyyy
                        @I_fecha            = @F_campo, -- yyyy/mm/dd
                        @I_fecha_conver     = @F_fecha output,
                        @I_formato          = @F_formato output -- 0= correcto, 1=incorrecto

                    if @F_formato = 1
                    begin
                        set @O_error_msg = 'El Campo ' + cast(@F_nro_campo as varchar(10)) + ' de la Fila =' + cast(@I_nro_linea as varchar(10)) +
                                        ' Debe tener el Formato de Fecha dd/mm/aaaa'
                        If @I_tipo_error = 1
                        Begin
                            --set @O_errores      = dbo.Fn_Adicionar_Error_PLA(@O_errores,'E8',',')
                            --set @O_Desc_errores = dbo.Fn_Adicionar_Error_PLA(@O_Desc_errores,'Error E8: '+ @O_error_msg,CHAR(13)+space(20))
                            --set @F_error_auto_aux=''
                            set @F_str_cod_error  = 'E8'
                            set @O_errores        = dbo.Fn_Adicionar_Error_PLA(@O_errores,@F_str_cod_error,',')
                            set @O_Desc_errores   = dbo.Fn_Adicionar_Error_PLA(@O_Desc_errores,'Error '+@F_str_cod_error+': '+ @O_error_msg,@CRLF)
                            set @F_error_auto_aux = ''
                            set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,ltrim(@I_nombre_archivo),'|')
                            set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,' ','|')
                            set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                            set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                            set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                            set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,@F_str_cod_error,'|')
                            set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,ltrim(@O_error_msg),'|')
                            set @F_error_auto     = dbo.Fn_Adicionar_Error_PLA(@F_error_auto, @F_error_auto_aux,@CRLF)
                        End
                        Else
                        GOTO linea_error
                    End
                End

                If @F_opcional = 2   --si es obligatorio
                    and LEN(LTRIM(rtrim(@F_campo)))=0
                Begin
                    set @O_error_msg = 'El Campo ' + cast(@F_nro_campo as varchar(10)) + ' de la Fila =' + cast(@I_nro_linea as varchar(10)) +
                                    ' Debe tener Fecha dd/mm/aaaa'
                    If @I_tipo_error = 1
                    Begin
                        --set @O_errores      = dbo.Fn_Adicionar_Error_PLA(@O_errores,'E9',',')
                        --set @O_Desc_errores = dbo.Fn_Adicionar_Error_PLA(@O_Desc_errores,'Error E9: '+ @O_error_msg,CHAR(13)+space(20))
                        set @F_str_cod_error  = 'E9'
                        set @O_errores        = dbo.Fn_Adicionar_Error_PLA(@O_errores,@F_str_cod_error,',')
                        set @O_Desc_errores   = dbo.Fn_Adicionar_Error_PLA(@O_Desc_errores,'Error '+@F_str_cod_error+': '+ @O_error_msg,@CRLF)
                        set @F_error_auto_aux = ''
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,ltrim(@I_nombre_archivo),'|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,' ','|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,@F_str_cod_error,'|')
                        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,ltrim(@O_error_msg),'|')
                        set @F_error_auto     = dbo.Fn_Adicionar_Error_PLA(@F_error_auto, @F_error_auto_aux,@CRLF)
                    End
                    Else
                        GOTO linea_error
                End
            end
        end

        update @temp
        set estado    = 1 -- se lo colocar en 1 a los campos que ya se leyeron
        Where campo     = @F_nro_campo
            and nro_linea = @z_nro_linea

        set @F_nro_campo += 1  --Incrementar
    End
End

-- Verificar si salio por Estructura
If @F_salir =1
    set @O_estructura = @F_salir

If exists ( select campo
            from @temp
            where estado = 0
                and nro_linea = @z_nro_linea )
begin
    set @O_error_msg = 'ERROR: De estructura en la fila= ' + cast(@I_nro_linea as varchar(10)) + cast(@z_nro_linea as varchar(10)) + cast(@I_tipo_error as varchar(10))

    If @I_tipo_error = 1
    Begin
        --set @O_errores      = dbo.Fn_Adicionar_Error_PLA(@O_errores,'E10',',')
        --set @O_Desc_errores = dbo.Fn_Adicionar_Error_PLA(@O_Desc_errores,'Error E10: '+ @O_error_msg,CHAR(13)+space(20))
        set @F_str_cod_error  = 'E10'
        set @O_errores        = dbo.Fn_Adicionar_Error_PLA(@O_errores,@F_str_cod_error,',')
        set @O_Desc_errores   = dbo.Fn_Adicionar_Error_PLA(@O_Desc_errores,'Error '+@F_str_cod_error+': '+ @O_error_msg,@CRLF)
        set @F_error_auto_aux = ''
        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,ltrim(@I_nombre_archivo),'|')
        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,' ','|')
        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,'','|')
        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,@F_str_cod_error,'|')
        set @F_error_auto_aux = dbo.Fn_Adicionar_Error_PLA(@F_error_auto_aux,ltrim(@O_error_msg),'|')
        set @F_error_auto     = dbo.Fn_Adicionar_Error_PLA(@F_error_auto, @F_error_auto_aux,@CRLF)
        set @O_estructura     = 1-- sale por estructura
    End
    else
        GOTO linea_error
end

set @O_linea_error_formato = @F_error_auto

return 0

linea_error:
    SET @O_error_msg = 'PA_Validar_estructura_ASCII.§' + @O_error_msg

    IF @@nestlevel = 1
        RAISERROR(@O_error_msg, 16, -1)

    RETURN -1
GO
