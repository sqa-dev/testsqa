﻿SET ANSI_NULLS, QUOTED_IDENTIFIER ON
GO
CREATE or alter procedure dbo.Registrar_desde_excel_ASCII
    @I_origen           tinyint = 0, -- 0=llamado desde pantalla 1=llamado desde el automatico
    @I_grabar           tinyint = 0,
    @I_idTabla          INT = 0,
    @I_nombre_archivo   varchar(50) = '',
    @I_Tabla            TypeTabla readonly,
    @I_error_formato    tinyint = 0,--0=sin formato, actual 1=registrar en la tabla con formato
    @O_archivo_errores  varchar(max) = '' output,
    @O_error_msg        varchar(400) = '' output
WITH ENCRYPTION
as
/**********************************************************************************************/
/* DESCRIPCIÓN: SP de carga de Archivos                                                       */
/* OBSERVACIÓN: RD-8837, indentacion, lectura a climov_usuario y add dbo. a los exec          */
/* REV.CALIDAD: OK                                                                            */
/**********************************************************************************************/
set nocount on
set XACT_ABORT ON

If @@NESTLEVEL = 1
    select nombre_reporte = 'SinReporte'

declare
    @F_idtabla              int,
    @F_nro_registro         int,
    @F_error                smallint,
    @F_rowcount             smallint,
    @F_Tipo_archivo         tinyint,
    @F_cant_titulo          smallint,
    @F_separadores          char(1),
    @F_cant_reg             int,
    @F_sec                  int,
    @F_error_exe            int,
    @F_tipo_linea           tinyint,
    @F_linea                varchar(max),
    @F_nombre_archivo       varchar(50), -- Cuando tiene nombre significa que Valide el nombre del archivio
    @F_nombre_hoja          varchar(50),  -- nombre de que hoja va leer si es de excel
    @F_nombre_sp            varchar(8000),
    @F_errores              varchar(max),
    @F_Desc_errores         varchar(max),
    @F_Desc_errores2        varchar(max),
    @F_tiene_error          tinyint,
    @F_mensaje              varchar (300),
    @F_nro_error            smallint,
    @F_estructura           tinyint ,
    @F_mostrar_errores      tinyint,
    @F_tipo                 tinyint,
    @tabla                  TypeTabla,
    @F_remesadora_aux       int,
    @F_nombre_remesa        varchar(50),
    @F_fecha_proceso        smalldatetime,
    @F_observacion_error    varchar(200),
    @F_fecha_desde_log      datetime,
    @F_usuario              int,
    @F_sistema              smallint,
    @F_primera_linea        varchar(max),
    @F_validar_estruc       tinyint


create table #Error_estruc (
    codigo           decimal(20,0) identity (1,1),
    secuencia        decimal(20,0),-- en que linea se encuentra el error
    nombre_archivo   varchar(50),
    linea            varchar(max),
    lista_error      varchar(max),
    desc_error       varchar(max),
    linea_error      varchar(max),--formateado con separador
    estructura       tinyint
)

truncate table #Error_estruc

select
    @F_fecha_desde_log = getdate(),
    @F_fecha_proceso   = (select fecha_proceso
                        from pam_fechaproceso
                        where indicador = 'A'
                            and sistema = 400),
    @F_usuario = isnull((select cliente
                        from climov_usuario
                        where fecha_proceso_hasta = '01-01-2050'
                            and indicador = 'A'
                            and nombcorto = cast(system_user as varchar(32))), 0)

If @F_usuario = 0
begin
    set @O_error_msg = 'El usuario no existe...Verifique'
    GOTO linea_error
end

--if @I_idTabla in(14,38) --carga de remesa
If exists ( select sec
            from girmst_remesadora
            where codigo_tabla = @I_idTabla
                and fecha_proceso_hasta ='01-01-2050'
                and indicador ='A')
    AND @I_origen = 0 /* modficado 18-05-2013 */
begin
    create table #remesas_SMS (
        codigo        decimal(16) IDENTITY(1, 1),
        remesadora    int,
        codigo_remesa varchar(50)
    )

    truncate table #remesas_SMS
end

If @@NESTLEVEL = 1 and @I_grabar =1
    and not exists(select * from pam_tablas where tabla= 489 and fecha_proceso_hasta='01-01-2050' and indicador ='A' and codigo=@I_idTabla)
begin tran
    If isnull(@I_idTabla,-1)<=0
    begin
        set @O_error_msg = 'Ingresar el código de la Tabla a Verificar'
        GOTO linea_error
    end

    Select
        @F_Tipo_archivo   = Tipo_archivo,   -- 0=ASCII
                                             -- 1=EXCEL
        @F_cant_titulo    = cant_titulo,    -- cantidad de titulo
        @F_separadores    = separadores,    -- En caso que sea ascii
        @F_nombre_archivo = nombre_archivo, -- Cuando tiene nombre significa que vaslida el nombre del archivio
        @F_nombre_hoja    = nombre_hoja ,   -- nombre de que hoja va leer si es de excel
        @F_nombre_sp      = nombre_sp,
        @F_mostrar_errores= mostrar_errores,
        @F_sistema = sistema
    from pam_tipoTabla
    where idTabla  = @I_idTabla
        and fecha_proceso_hasta = '01-01-2050'
        and indicador = 'A'

    Select @F_error =@@ERROR, @F_rowcount =@@ROWCOUNT

    If @F_error <> 0 Or @F_rowcount > 1
    begin
        set @O_error_msg = 'Error al leer la tabla enviada error= ' + STR(@F_error,2)+ ' rowcount= ' + STR(@F_rowcount, 2)
        GOTO linea_error
    end

    If @F_rowcount = 0
    begin
        set @O_error_msg = 'La tabla indicada debe tener parámetro definido'
        GOTO linea_error
    end
    -- Validar el Nombre del SP
    IF not exists ( select name
                    from sys.all_objects
                    where name = @F_nombre_sp
                        and type ='P' )
    Begin
        set @O_error_msg = 'El procedimiento= '+ ltrim(@F_nombre_sp)+' no existe ' +STR(@I_idTabla)
        GOTO linea_error
    End
    ---
    delete @tabla

    Insert into @tabla
    select
        idtabla,
        nro_linea,
        linea,
        error_estruct= 0
    from @I_Tabla
    where idTabla = @I_idTabla

    select
        @F_cant_reg     = COUNT(1),
        @F_sec          = 1,
        @F_tiene_error  = 0
    from @tabla where idTabla = @I_idTabla

    delete #Error_estruc

    set @F_validar_estruc=1

    IF @I_idTabla=29 and @F_cant_reg >0
    begin
        --Obtenemos la Primera línea del archivo
        select top 1 @F_primera_linea =linea
        from @I_Tabla

        if exists ( select descripcion
                    from pam_tablas
                    where tabla = 571
                        and indicador = 'A'
                        and fecha_proceso_hasta = '01-01-2050'
                        and PATINDEX(descripcion + '%', @F_primera_linea) > 0)
            set @F_validar_estruc=0
    end

     While @F_sec < = @F_cant_reg and @F_validar_estruc = 1
    Begin
        Select @F_linea  = linea
        from @tabla
        --from @I_Tabla
        Where nro_linea = @F_sec
        -- VALIDAR ESTRUCTURA DE ASCII
        set @F_errores      = ''
        set @F_Desc_errores = '         '
        set @F_Desc_errores2= ''
        set @F_nro_error    = 0
        set @F_estructura   = 0-- 0= Error no es estructura 1=error de estructura
        set @F_tipo         =  case when @F_mostrar_errores = 1 then 0
                                                                else 1 end

        EXEC @F_error_exe = dbo.Validar_estructura_ASCII
            @I_nombre_archivo      = @I_nombre_archivo,
            @I_tipo_error          = @F_tipo ,--0 = valida campo x campo
                                                --1 = devuelve los errores en una tabla
            @I_idtabla             = @I_idTabla,
            @I_Tipo_archivo        = @F_Tipo_archivo,
            @I_cant_titulo         = @F_cant_titulo,
            @I_separador           = @F_separadores,
            @I_nro_linea           = @F_sec, -- el nro de la linea
            @I_linea               = @F_linea,-- el detalle de la linea
            @O_errores             = @F_errores       output,
            @O_nro_error           = @F_nro_error     output,
            @O_Desc_errores        = @F_Desc_errores  output,
            @O_linea_error_formato = @F_Desc_errores2 output,
            @O_estructura          = @F_estructura    output,
            @O_error_msg           = @O_error_msg     output

        If @@ERROR <>0
        Begin
            set @O_error_msg = 'Error al Ejecutar Validar_estructura_ASCII'
            GOTO linea_error
        End

        If @F_error_exe <>0
            GOTO linea_error

        -- Insertar los errores
        If @F_mostrar_errores in(2,3)--Inserta cuando se selecciono
        Begin
            If Len(LTRIM(rtrim(@F_errores)))>0
            Begin
                set @F_tiene_error = 1

                Insert into #Error_estruc
                select
                    secuencia     = @F_sec,
                    nombre_archivo= @I_nombre_archivo,
                    linea         = @F_linea,
                    lista_error   = @F_errores,
                    desc_error    = @F_Desc_errores,
                    linea_error   = @F_Desc_errores2,
                    estructura    = @F_estructura

                If @@ERROR <> 0
                begin
                    set @O_error_msg = 'Error al insertar en la tabla #Error_estruc para el reporte de Errores'
                    goto linea_error
                end

                If @F_estructura >0
                begin
                    update @tabla
                    set error_estruct = 1
                    Where nro_linea = @F_sec
                end
            End--Len(LTRIM(rtrim(@F_lista_error)))>0
        End

        Set @F_sec += 1 -- INCREMENTAR
    End

    If @F_mostrar_errores = 2 --MOSTRAR ERRORES DE ESTRUCTURA EN UN REPORTE
        and @I_origen = 0--llamdo desde pantalla
    Begin
        If @I_grabar = 0 -- Mostrar la lista de Errores
            and @F_tiene_error > 0
        Begin
            Select Linea_texto ='E' + '1'
            set @F_mensaje = 'No se ha realizado la carga del archivo. Verificar'
            select Linea_texto ='M' + @F_mensaje
            select Linea_texto ='R'

            exec dbo.proc_reppam_error_estructura_archivo
                @I_nombre_archivo    = @I_nombre_archivo
            --drop table #Error_estruc
            return 0
        End
    end
    -- Llamar el Procedimineto que valida los datos del archivo
    If ( (@I_grabar = 1 and Len(LTRIM(rtrim(@F_errores))) = 0 and @F_mostrar_errores in(1, 2))
        Or  (@F_mostrar_errores = 3) )
    Begin
        DECLARE
            @sql             nvarchar(1000),
            @paramDefinition nvarchar(255)

        SET @paramDefinition = ' @I_grabar tinyint, @I_Tabla TypeTabla readonly, @I_nombre_archivo varchar(50), @O_error_msg varchar(400) OUTPUT'
        Set @sql = @F_nombre_sp + N' @I_grabar, @I_Tabla , @I_nombre_archivo, @O_error_msg output '
        set @O_error_msg =''

        EXEC sp_executesql
            @sql,
            @paramDefinition,
            @I_grabar         = @I_grabar,
            @I_Tabla          = @tabla,
            @I_nombre_archivo = @I_nombre_archivo,
            @O_error_msg      = @O_error_msg OUTPUT

        if @F_mostrar_errores <> 3
        begin
           set @O_error_msg = isnull(@O_error_msg, '')

           If len(LTRIM(@O_error_msg)) > 0
            GOTO linea_error
        end
    End

    If @F_mostrar_errores = 3 -- Mostrar el error en un solo reporte
        and (select COUNT(1) from #Error_estruc) > 0
    Begin
        If @I_grabar = 0
            and @I_origen = 0 -- cuando sea llamado desde pantalla
        Begin
            Select Linea_texto = 'E' + '1'
            set @F_mensaje     = 'No se ha realizado la carga del archivo. Verificar'
            select Linea_texto = 'M' + @F_mensaje
            select Linea_texto = 'R'

            exec dbo.proc_reppam_error_estructura_archivo
                @I_nombre_archivo    = @I_nombre_archivo
        End
        ---
        If @I_grabar in (1,0)
            and @I_origen = 1 -- si se lo llama del proceso automatico
        Begin
            exec dbo.proc_reppam_error_estructura_archivo
                @I_archivo           = 1,
                @I_error_formato     = @I_error_formato,--0=sin formato, actual 1=registrar en la tabla con formato
                @I_nombre_archivo    = @I_nombre_archivo,
                @I_sistema           = @F_sistema,
                @O_archivo           = @O_archivo_errores output,
                @O_error_msg         = @O_error_msg OUTPUT

            If @@ERROR <>0
            begin
                set @O_error_msg = 'Error al insertar en la tabla #Error_estruc para el reporte de Errores'
                goto linea_error
            end
        End --@I_grabar = 1  and @I_origen = 1

        return 0
    End
    -- CUANDO NO TIENE ERROR Y ES LLAMADO DESDE PANTALLA
    If @I_grabar =1
        and (select COUNT(1) from #Error_estruc) = 0 and len(ltrim(@O_error_msg)) = 0
        and @I_origen = 0 --cuando es llamdo desde pantalla
    begin
        Select Linea_texto  ='M'+'Transacción OK.'
    end
    -- CUANDO NO TIENE ERROR Y ES LLAMADO DESDE EL AUTOMATICO
    If @I_grabar =1
        and (select COUNT(1) from #Error_estruc) = 0 and len(ltrim(@O_error_msg)) = 0
        and @I_origen = 1 --cuando es llamdo desde el automatico
    begin
        set @O_archivo_errores = ''
    end

    --drop table #Error_estruc
    if dbo.fn_gbl_existe_objeto(2,'#Error_estruc')=1 drop table #Error_estruc

Salir_procedimiento:
    If @@NESTLEVEL  = 1 and @I_grabar =1
        and not exists(select 1 from pam_tablas where tabla= 489 and fecha_proceso_hasta = '01-01-2050' and indicador = 'A' and codigo = @I_idTabla)
    begin
        commit tran
    end
    --ENVIAR LOS MENSAJES RECIEN CARGADO
    --if @I_idTabla in(14,38) --carga de remesa
    If exists ( select sec
                from girmst_remesadora
                where codigo_tabla = @I_idTabla
                    and fecha_proceso_hasta = '01-01-2050'
                    and indicador = 'A')
        AND @I_origen = 0 /* modficado 18-05-2013 */
        and @I_grabar = 1
    begin
        set @F_sec      = 1
        set @F_cant_reg = (select COUNT(1) from #remesas_SMS)

        while @F_sec<= @F_cant_reg
        begin
            select
                @F_remesadora_aux =remesadora,
                @F_nombre_remesa = codigo_remesa
            from #remesas_SMS
            where codigo = @F_sec
            -----------------------------------------------------------------------
            -- REALIZAR EL ENVIO DE LOS SMS A LOS REGISTROS DEL ARCHIVOS CARGADO
            -----------------------------------------------------------------------
            exec dbo.proc_abmgir_enviar_sms_automatico_carga
                @I_grabar                = 1,
                @I_fecha_proceso         = @F_fecha_proceso,
                @I_remesadora            = @F_remesadora_aux,
                @I_codigo_remesa         = @F_nombre_remesa,
                @O_error_msg             = @O_error_msg output
            set @F_sec  +=1
        end
        -- drop table #remesas_SMS
        if dbo.fn_gbl_existe_objeto(2, '#remesas_SMS') = 1 drop table #remesas_SMS
    end
    ---------------------------------------------------------------------------------------------------
    --  Grabar log
    ---------------------------------------------------------------------------------------------------
    set @F_observacion_error = 'Origen:' + isnull(ltrim(cast(@I_origen as varchar(1))), 'Nulo') + '; ' +
                              'idTabla:' + isnull(ltrim(cast(@I_idTabla as varchar(10))), 'Nulo') + '; ' +
                              'NombreArchivo:' + ltrim(rtrim(isnull(@I_nombre_archivo, 'Nulo')))

    exec dbo.Grabar_log_GLB
                @I_usuario            = @F_usuario,
                @I_fecha_proceso      = @F_fecha_proceso,
                @I_observacion_error  = @F_observacion_error,
                @I_nombre_sp          = 'Registrar_desde_excel_ASCII',  --Nombre del procedimiento almacenado
                @I_fecha_desde        = @F_fecha_desde_log,
                @I_tipo_log           = 2,                              --1=reporte, 2=abm
                @I_es_error           = 0,                              --0=no es error, 1=si es error
                @I_error_msg          = ''

    return 0

linea_error:
    --drop table #Error_estruc
    if dbo.fn_gbl_existe_objeto(2, '#Error_estruc') = 1 drop table #Error_estruc
    --if @I_idTabla in(14,38) --carga de remesa

    If exists ( select sec
                from girmst_remesadora
                where codigo_tabla= @I_idTabla
                    and fecha_proceso_hasta = '01-01-2050'
                    and indicador = 'A')
        AND @I_origen = 0 /* modficado 18-05-2013 */
    begin
        -- drop table #remesas_SMS
        if dbo.fn_gbl_existe_objeto(2, '#remesas_SMS') = 1 drop table #remesas_SMS
    end

    SET @O_error_msg = 'Registrar_desde_excel_ASCII.§' + rtrim(isnull(@O_error_msg, 'Nulo'))

    if  @@NESTLEVEL = 1
    begin
        if @I_grabar = 1
            and not exists(select 1 from pam_tablas where tabla= 489 and fecha_proceso_hasta='01-01-2050' and indicador ='A' and codigo=@I_idTabla)
        begin
            rollback tran
        end

        ---------------------------------------------------------------------------------------------------
        --  Grabar log
        ---------------------------------------------------------------------------------------------------
        set @F_observacion_error = 'Origen:' + isnull(ltrim(cast(@I_origen as varchar(1))), 'Nulo') + '; ' +
                                'idTabla:' + isnull(ltrim(cast(@I_idTabla as varchar(10))), 'Nulo') + '; ' +
                                'NombreArchivo:' + ltrim(rtrim(isnull(@I_nombre_archivo, 'Nulo')))

        exec dbo.Grabar_log_GLB
            @I_usuario            = @F_usuario,
            @I_fecha_proceso      = @F_fecha_proceso,
            @I_observacion_error  = @F_observacion_error,
            @I_nombre_sp          = 'Registrar_desde_excel_ASCII',  --Nombre del procedimiento almacenado
            @I_fecha_desde        = @F_fecha_desde_log,
            @I_tipo_log           = 2,                --1=reporte, 2=abm
            @I_es_error           = 1,                --0=no es error, 1=si es error
            @I_error_msg          = @O_error_msg

    end

    IF @@nestlevel = 1
        RAISERROR(@O_error_msg, 16, -1)

    RETURN -1
GO