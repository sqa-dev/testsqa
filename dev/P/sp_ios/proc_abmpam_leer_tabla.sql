﻿SET ANSI_NULLS, QUOTED_IDENTIFIER ON
GO
CREATE or alter procedure dbo.proc_abmpam_leer_tabla
    @I_idTabla          int          = 0,
    @I_nombre_tabla     varchar(100) = '',
    @I_nombre_archivo   varchar(200) = '',
    @O_error_msg        varchar(300) = '' output
WITH ENCRYPTION
AS
/**************************************************************************************/
/* DESCRIPCIÓN: Permite leer y validar los datos del id tabla                         */
/*               programa indicara que head tiene que crear                           */
/* OBSERVACIÓN: RD-8837 IV, indentacion, lectura climov_usuario, add dbo. a los exec  */
/* REV.CALIDAD: OK                                                                    */
/**************************************************************************************/
set nocount on
set XACT_ABORT ON

declare
    @F_rowcount       int,
    @F_error          int,
    --@F_error_msg      varchar(300),
    @F_fecha_proceso  smalldatetime,
    @F_fecha_today    datetime,
    @F_usuario        int,
    @F_str_remesadora varchar(100),
    @F_str_idtabla    varchar(100),
    @F_remesadora     int,
    @F_nombre_archivo varchar(50),
    @fecha            varchar(50),
    @F_aux            varchar(8000),
    @F_cant_reg       int,
    @F_error_exec     int

select
    @F_fecha_proceso = (select fecha_proceso
                        from pam_fechaproceso
                        where indicador = 'A'
                            and sistema = 400),
    @F_fecha_today   = getdate(),
    @F_usuario = isnull((select cliente
                        from climov_usuario
                        where fecha_proceso_hasta = '01-01-2050'
                            and indicador = 'A'
                            and nombcorto = cast(system_user as varchar(32))), 0)

Declare
    @tabla table (
        id_tabla    int,
        codigo      int,
        idpublica   varchar(50),
        rutaenvio   varchar(50),
        correo      varchar(50),
        contraseña  varchar(50)
    )

If isnull(@I_idTabla, -1) <= 0
begin
    set @O_error_msg = 'Ingresar el Id_tabla'
    goto error
end
--AUMENTADO POR GABRIEL
--Select
--    @F_input=input,
--    @F_sentencia = sentencia
--from pam_parametro_tabla
--Where id_tabla = @I_idTabla
--    and fecha_proceso_hasta= '01-01-2050'
--    and indicador ='A'
--declare
--    @F_sentencia  varchar(100)=' select ¶F_str_idtabla '+' , '+ ' remesadora ,idpublica,rutaenvio,correo, contraseña '+
--                               ' from ¶I_nombre_tabla '+
--                               ' where remesadora = ¶F_str_remesadora ' +
--                                   ' and fecha_proceso_hasta = ''01-01-2050'' ' +
--                                   ' and indicador =''A'' ',
--    @F_input           varchar(100)= '¶F_str_idtabla,¶I_nombre_tabla,',
--    @z_input           varchar(100)= '',
--    @F_id_tabla        int=5,
--    @F_str_id_tabla    varchar(20)='',
--    @F_condicion       varchar(100)='1=1',
--    @F_sentenciaaux    varchar(200)=''
--While len(@F_input)>0
--begin
--    set @z_input        = cast(SUBSTRING(@F_input,1,charindex(',',@F_input)-1)as varchar(100))
--    set @F_input        = substring(@F_input,charindex(',',@F_input)+1,len(@F_input))
--    set @F_str_id_tabla = ltrim(str(@F_id_tabla))
--    set @F_aux          = REPLACE(@F_sentencia,@z_input,@F_str_id_tabla)
--end
--select REPLACE('aasjkdahkd','a','UUU')
set @F_str_idtabla = ltrim(STR(@I_idTabla))

If @I_idTabla in (6, 14) -- si el el formato de remesadora
Begin
    -- VALIDAR EL FORMATO DE REMESA
    set @fecha = replace(convert(char(10), @F_fecha_today, 105), '-', '')

    If charindex(@fecha,@I_nombre_archivo) <= 0
    begin
        set @O_error_msg = 'El Nombre del Archivo=' + rtrim(ltrim(@I_nombre_archivo)) + ' no se encuentra en el Formato correcto --> codigoremesadora+fecha+sec.extension'
        goto error
    end

    set @F_str_remesadora = ltrim(cast(SUBSTRING(@I_nombre_archivo, 1, charindex(@fecha,@I_nombre_archivo) -1)as varchar(100)))

    If len(ltrim(@F_str_remesadora)) = 0
    begin
        set @O_error_msg = 'El formato del Nombre Archivo '+ rtrim(ltrim(@I_nombre_archivo)) + ' es Incorrecto --> codigoremesadora+fecha+sec.extension...'
        goto error
    end

    If isnumeric(@F_str_remesadora) <> 1 -- Si no es Numerico
    begin
        set @O_error_msg = 'No es Numerico el codigo de la remesadora del Nombre del Archivo '+ rtrim(ltrim(@I_nombre_archivo))+' es Incorrecto --> codigoremesadora+fecha+sec.extension'
        goto error
    end

    set @F_remesadora  = CAST( @F_str_remesadora as int)

    -- VALIDAR LA REMESADORA
    exec @F_error_exec = dbo.proc_gir_validar_remesadora
        @I_origen           = 1,--1=llamado desde la carga de archivo 0=llamdo desde otraas validaciones
        @I_fecha_proceso    = @F_fecha_proceso,
        @I_remesadora       = @F_remesadora,
        @O_error_msg        = @O_error_msg output

    if @@error <> 0
    begin
        set @O_error_msg = 'error en exec proc_abmgir_validar_remesadora'
        goto Error
    end

    If @F_error_exec <> 0
        goto Error

    --set @F_nombre_archivo = dbo.Fn_gir_obtener_nombArchivo_giro(@F_remesadora,@F_fecha_today,0)

    If exists (select remesadora from girmst_head where ltrim(nombre_archivo)  =ltrim(@I_nombre_archivo) ) --charindex(@F_nombre_archivo,@I_nombre_archivo)<=0
    begin
        set @O_error_msg = 'El nombre de Archivo se encuentra registrado---> remesadora+fecha+sec.extension' +LTRIM(RTRIM(@I_nombre_archivo))
        goto error
    end

    set @F_aux = (' select '+ @F_str_idtabla + ' ,remesadora ,idpublica,rutaenvio,correo, dbo.fn_Desencript_clave(contraseña) ' +
                ' from ' + @I_nombre_tabla +
                ' where remesadora = ' + @F_str_remesadora+
                    ' and fecha_proceso_hasta = ''01-01-2050'' ' +
                    ' and indicador =''A'' ')

    Insert into @tabla
    exec (@F_aux)

    select @F_cant_reg = COUNT(1)
    from @tabla

    If @F_cant_reg <= 0
    begin
        set @O_error_msg = 'No se ha cargado la llave pública, en la configuración de la empresa remesadora en el Sificon'
        goto error
    end
end

--If @I_idTabla in(16) -- Remesa
--Begin
--    set @F_str_idtabla = ltrim(STR(@I_idTabla))
--    set @F_aux = ( ' select '+ @F_str_idtabla +','+@F_str_idtabla+',idpublica,rutaenvio,'''','''' from ' + @I_nombre_tabla +
--                  ' where fecha_proceso_hasta = ''01-01-2050'' ' +
--                  '   and indicador =''A'' ')
----select 'aqui'  +@F_aux
--end

-- Ejecutar la consulta
select
    codigo,
    idpublica,
    rutaenvio,
    correo,
    contraseña
from @tabla
where id_tabla = @I_idTabla

Return 0

Error:
    set @O_error_msg = 'PA_proc_abmpam_leer_tabla.§' + rtrim(@O_error_msg)

    if @@nestlevel = 1
        raiserror(@O_error_msg, 16, -1)

    Return -1
GO
