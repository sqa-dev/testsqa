﻿SET ANSI_NULLS, QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER FUNCTION dbo.fnt_not_obtener_tabla_cabecera (
    @I_id_tabla int
) RETURNS
    @Cabecera TABLE (
        idTabla int,
        campo1  varchar(300),      campo2  varchar(300),      campo3  varchar(300),      campo4  varchar(300),      campo5  varchar(300),
        campo6  varchar(300),      campo7  varchar(300),      campo8  varchar(300),      campo9  varchar(300),      campo10 varchar(300),
        campo11 varchar(300),      campo12 varchar(300),      campo13 varchar(300),      campo14 varchar(300),      campo15 varchar(300),
        campo16 varchar(300),      campo17 varchar(300),      campo18 varchar(300),      campo19 varchar(300),      campo20 varchar(300),
        campo21 varchar(300),      campo22 varchar(300),      campo23 varchar(300),      campo24 varchar(300),      campo25 varchar(300),
        campo26 varchar(300),      campo27 varchar(300),      campo28 varchar(300),      campo29 varchar(300),      campo30 varchar(300),
        campo31 varchar(300),      campo32 varchar(300),      campo33 varchar(300),      campo34 varchar(300),      campo35 varchar(300),
        campo36 varchar(300),      campo37 varchar(300),      campo38 varchar(300),      campo39 varchar(300),      campo40 varchar(300),
        campo41 varchar(300),      campo42 varchar(300),      campo43 varchar(300),      campo44 varchar(300),      campo45 varchar(300),
        campo46 varchar(300),      campo47 varchar(300),      campo48 varchar(300),      campo49 varchar(300),      campo50 varchar(300)
    )
 WITH ENCRYPTION
 AS
 /**********************************************************************************************/
 /* DESCRIPCIÓN: Retorna una tabla con los campos de la cabecera de la carga de un archivo     */
 /* OBSERVACIÓN: RD-8837 IV, identacion de codigo                                              */
 /* REV.CALIDAD: OK                                                                            */
 /**********************************************************************************************/
BEGIN
    INSERT INTO @Cabecera
    select
        idTabla,
        campo1  = isnull(MAX([campo1] ),''),    campo2  = isnull(MAX([campo2] ),''),    campo3  = isnull(MAX([campo3] ),''),    campo4  = isnull(MAX([campo4] ),''),
        campo5  = isnull(MAX([campo5] ),''),    campo6  = isnull(MAX([campo6] ),''),    campo7  = isnull(MAX([campo7] ),''),    campo8  = isnull(MAX([campo8] ),''),
        campo9  = isnull(MAX([campo9] ),''),    campo10 = isnull(MAX([campo10]),''),    campo11 = isnull(MAX([campo11]),''),    campo12 = isnull(MAX([campo12]),''),
        campo13 = isnull(MAX([campo13]),''),    campo14 = isnull(MAX([campo14]),''),    campo15 = isnull(MAX([campo15]),''),    campo16 = isnull(MAX([campo16]),''),
        campo17 = isnull(MAX([campo17]),''),    campo18 = isnull(MAX([campo18]),''),    campo19 = isnull(MAX([campo19]),''),    campo20 = isnull(MAX([campo20]),''),
        campo21 = isnull(MAX([campo21]),''),    campo22 = isnull(MAX([campo22]),''),    campo23 = isnull(MAX([campo23]),''),    campo24 = isnull(MAX([campo24]),''),
        campo25 = isnull(MAX([campo25]),''),    campo26 = isnull(MAX([campo26]),''),    campo27 = isnull(MAX([campo27]),''),    campo28 = isnull(MAX([campo28]),''),
        campo29 = isnull(MAX([campo29]),''),    campo30 = isnull(MAX([campo30]),''),    campo31 = isnull(MAX([campo31]),''),    campo32 = isnull(MAX([campo32]),''),
        campo33 = isnull(MAX([campo33]),''),    campo34 = isnull(MAX([campo34]),''),    campo35 = isnull(MAX([campo35]),''),    campo36 = isnull(MAX([campo36]),''),
        campo37 = isnull(MAX([campo37]),''),    campo38 = isnull(MAX([campo38]),''),    campo39 = isnull(MAX([campo39]),''),    campo40 = isnull(MAX([campo40]),''),
        campo41 = isnull(MAX([campo41]),''),    campo42 = isnull(MAX([campo42]),''),    campo43 = isnull(MAX([campo43]),''),    campo44 = isnull(MAX([campo44]),''),
        campo45 = isnull(MAX([campo45]),''),    campo46 = isnull(MAX([campo46]),''),    campo47 = isnull(MAX([campo47]),''),    campo48 = isnull(MAX([campo48]),''),
        campo49 = isnull(MAX([campo49]),''),    campo50 = isnull(MAX([campo50]),'')
    FROM (
        SELECT
            idTabla,
            nombre_titulo,
            PValor_campo = 'campo' + CAST(DENSE_RANK() OVER (PARTITION BY idTabla ORDER BY secuencia ASC) AS NVARCHAR)
        FROM pam_tipoTablaDetalle p
        where idTabla = @I_id_tabla
            and indicador = 'A'
            and fecha_proceso_hasta = '01-01-2050'
    ) AS query
    PIVOT (
        MAX(nombre_titulo)
            FOR PValor_campo IN (
                    [campo1] , [campo2] , [campo3] , [campo4] , [campo5] , [campo6] , [campo7] , [campo8] , [campo9] , [campo10],
                    [campo11], [campo12], [campo13], [campo14], [campo15], [campo16], [campo17], [campo18], [campo19], [campo20],
                    [campo21], [campo22], [campo23], [campo24], [campo25], [campo26], [campo27], [campo28], [campo29], [campo30],
                    [campo31], [campo32], [campo33], [campo34], [campo35], [campo36], [campo37], [campo38], [campo39], [campo40],
                    [campo41], [campo42], [campo43], [campo44], [campo45], [campo46], [campo47], [campo48], [campo49], [campo50]
                )
    ) AS Pivot1
    GROUP BY idTabla
    ORDER BY idTabla

    RETURN
END
GO
