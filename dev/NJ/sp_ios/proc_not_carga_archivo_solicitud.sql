﻿SET ANSI_NULLS, QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE dbo.proc_not_carga_archivo_solicitud
    @I_grabar         int          = 0,
    @I_Tabla          TypeTabla         readonly,
    @I_nombre_archivo varchar(50)  = ''  ,
    @O_error_msg      varchar(600) = '' output
WITH ENCRYPTION
as
/**********************************************************************************************/
/* DESCRIPCIÓN: Permite cargar los datos del archivo de una solicitud                         */
/* OBSERVACIÓN: RD-8837 IV, Optimización en el proceso proc_abmnot_procesar_carga             */
/* REV.CALIDAD: OK                                                                            */
/**********************************************************************************************/
set nocount on
set XACT_ABORT ON -- Notificamos al Motor que si ocurre algun error en la Transaccion lo Revierta.

if @@trancount > 0 and @@NESTLEVEL = 1 -- Si existe alguna Transaccion activa en esta Session lo Revertimos.
    rollback tran

if @@NESTLEVEL = 1
    select nombre_reporte = 'SinReporte'

declare
    @F_nuevo               tinyint       = 1, -- 1= si es nuevo, 0= no es nuevo
    @F_id_tabla            int           = 0,
    @F_nro_carga           decimal(10),
    @F_usuario             int           = 0,
    @F_mensaje             varchar(150)  = '',
    @F_error               smallint      = 0,
    @F_error_exec          smallint      = 0,
    @F_fecha_today         smalldatetime = '01-01-1900',
    @F_fecha_proceso       smalldatetime = '01-01-1900',
    @F_fecha_desde_log     datetime      = '01-01-1900',
    @F_observacion_error   varchar(200)  = '',
    @F_fecha_proceso_hasta smalldatetime = '01-01-1900',
    --
    @F_ctrl_limite_proceso int,
    @F_ctrl_hora_inicio    varchar(8),
    @F_ctrl_hora_fin       varchar(8),
    @F_ctrl_habilitado     tinyint,
    @F_ctrl_ejecutado      bit = 0,
    --
    @F_cant_reg            int           = 0,
    @F_fecha               date          = '',
    @F_autoridad           varchar(100)  = '',
    @F_codigo_notificacion varchar(40)   = '',
    @F_tipo_solicitud      tinyint       = 0 ,
    @F_ente                varchar(15)   = ''

create table #t_datos_archivo (
    codigo_notificacion varchar(40),
    codigo_solicitud    varchar(40),
    nro_carga           decimal(9),
    sec_carga           int,
    cod_campo           int,
    valor_campo         varchar(200)
)

create nonclustered index #i_t_datos_archivo1 ON #t_datos_archivo (cod_campo) include (sec_carga, valor_campo)
create nonclustered index #i_t_datos_archivo2 ON #t_datos_archivo (sec_carga)

create table #T_error(
    secuencia int identity,
    fila      int,       --numero de fila del excel
    nro_campo int,       --numero del campo del excel
    cod_error int        --numero de error
)

--tabla para validar si exiten datos duplicados por linea
create table #T_tabla_error (
    idtabla       int,
    nro_linea     int,
    linea         varchar(max),
    cod_error     int
)

-----------------------------
-- INICIALIZAMOS VARIABLES --
-----------------------------
select
    @F_fecha_today     = GETDATE(),
    @F_fecha_desde_log = GETDATE()

select @F_id_tabla = codigo
from pam_tablas
where tabla = 489
    and fecha_proceso_hasta = '01-01-2050'
    and indicador = 'A'
    and codigo = 97 -- COD TABLA ASIGNADO

if (@@NESTLEVEL = 1 and @I_grabar = 1 )
    OR (@@NESTLEVEL > 1 and @I_grabar = 1 AND @F_id_tabla > 0)
BEGIN
    BEGIN TRAN
END

exec @F_error_exec = dbo.proc_validar_usuario_fecha_proceso
    @I_sistema               = 400,
    @O_usuario               = @F_usuario             output,
    @O_fecha_proceso         = @F_fecha_proceso       output,
    @O_fecha_today           = @F_fecha_today         output,
    @O_fecha_proceso_hasta   = @F_fecha_proceso_hasta output,
    @O_error                 = @F_error               output,
    @O_error_msg             = @O_error_msg           output

if @F_error_exec <> 0
    goto linea_error


if @I_grabar = 0
begin
    select @F_cant_reg = count(1)
    from @I_Tabla a
    where a.idTabla = @F_id_tabla
        and a.nro_linea > 1

    --Inicio Validar Control de Limite de Proceso
    select
        @F_ctrl_limite_proceso  = [LIMITE_PROCESO],
        @F_ctrl_hora_inicio     = [HORA_INICIO],
        @F_ctrl_hora_fin        = [HORA_FIN],
        @F_ctrl_habilitado      = [HABILITADO]
    from (
        select
            clave = sigla,
            valor = sigla2
        from pam_tablas t
        where t.tabla = 986
            and t.fecha_proceso_hasta = '01-01-2050'
            and t.indicador = 'A'
            and t.sigla in ('LIMITE_PROCESO', 'HORA_INICIO', 'HORA_FIN', 'HABILITADO')
    ) as t
    pivot (
        max(valor)
            for clave in ([LIMITE_PROCESO], [HORA_INICIO], [HORA_FIN], [HABILITADO])
    ) as pvt

    if @F_ctrl_habilitado = 1
        and cast(getdate() as time) between @F_ctrl_hora_inicio and @F_ctrl_hora_fin
        and @F_cant_reg > @F_ctrl_limite_proceso
    begin
        set @F_ctrl_ejecutado = 1
        set @O_error_msg = 'En horario de ' + @F_ctrl_hora_inicio + ' a ' + @F_ctrl_hora_fin + ', solo se puede procesar hasta ' + cast(@F_ctrl_limite_proceso as varchar(10)) + ' registros. La carga actual tiene ' + cast(@F_cant_reg as varchar(10)) + ' registros.'

        goto linea_error
    end
    --Fin Validar Control de Limite de Proceso
end

set @F_mensaje = 'Está realizando una nueva carga.'

select linea_texto = 'T', @F_mensaje + CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10)

if @I_grabar = 1
BEGIN
    ----------------------
    -- VOLCADO DE TABLA --
    ----------------------
    truncate table #t_datos_archivo

    ;with
        #tmp_tabla as (
            select
                nro_linea   = a.nro_linea - 1,                                                         --fila
                nro_campo   = row_number() OVER (partition BY a.nro_linea ORDER BY a.nro_linea ASC),   --nro_campo
                valor_campo = value                                                                    --valor_campo
            from @I_Tabla a
                CROSS APPLY string_split(a.linea, '|')
            where a.idTabla = @F_id_tabla
                and a.nro_linea > 1 --(1er fila tiene la cebecera).
        )
    insert into #t_datos_archivo
    select
        codigo_notificacion = '',
        codigo_solicitud    = '',
        nro_carga           = 0,
        sec_carga           = a.nro_linea,
        cod_campo           = a.nro_campo,
        valor_campo         = UPPER(RTRIM(LTRIM(a.valor_campo)))
    from #tmp_tabla a
    order by a.nro_linea asc, a.nro_campo asc

    -----------------------------------------------------------
    -- OBTENEMOS [ENTE | FECHA | CIRCULAR | S/N] DE LA CARGA --
    -----------------------------------------------------------
    select @F_ente = valor_campo
    from #t_datos_archivo
    where cod_campo = 3  --ENTE
        and sec_carga = 1

    select @F_fecha = valor_campo
    from #t_datos_archivo
    where cod_campo = 4  --FECHA
        and sec_carga = 1

    select @F_codigo_notificacion = valor_campo
    from #t_datos_archivo
    where cod_campo = 5  --CIRCULAR
        and sec_carga = 1

    select  @F_tipo_solicitud = valor_campo
    from #t_datos_archivo
    where cod_campo = 6 -- S/N (TIPO DE SOLICITUD)
        and sec_carga = 1

    update #t_datos_archivo
    set codigo_notificacion = @F_codigo_notificacion

    ---------------------
    -- MAPA DE ERRORES --
    ---------------------
    --(1 ,'EL VALOR NO ES ENTERO POSITIVO'),
    --(2 ,'LOS VALORES DE LA FILA COMPLETA SE ENCUENTRA DUPLICADA EN EL DOCUMENTO'),
    --(3 ,'LOS VALORES DE LA COLUMNA SON DIFERENTES'),
    --(6 ,'LA CIRCULAR YA SE ENCUENTRA REGISTRADA CON ESTADO ACTIVO'),
    --(7 ,'VALOR INVÁLIDO, DEBE SER JURÍDICO O RELLENAR AP. PATERNO, AP. MATERNO, NOMBRE'),
    --(8 ,'EL VALOR NO SE ENCUENTRA PARAMETRIZADO EN LA TABLA [pamnot_dato_clave]'),
    --(9 ,'EL VALOR NO SE ENCUENTRA PARAMETRIZADO EN LA TABLA [pam_moneda] (BS., $US, UFV)'),
    --(10,'EL VALOR NO SE ENCUENTRA PARAMETRIZADO EN LA TABLA [pamnot_ente]'),
    --(11,'EL VALOR NO SE ENCUENTRA PARAMETRIZADO EN LA TABLA [pam_identificacion]'),
    --(15,'EXISTE TIPO DE SOLICITUD DISTINTA Y DEBE SER ÚNICA'),
    --(16,'EL VALOR DEBE SER (1 = RETENCIÓN o 2 = LIBERACIÓN) PARA INDICAR EL TIPO DE NOTIFICACION '),
    --(17,'EL VALOR NO CUMPLE CON EL FORMATO DD/MM/AA'),
    --(18,'EL VALOR NO DEBE ESTAR VACIO'),
    --(19,'DEBE SER JURIDICO O PERSONA NATURAL'),
    --(20,'NO PUEDE TENER EXTENCION SI ES JURIDICO'),
    --(21,'VALORES INVÁLIDOS, COMPLETAR O ELIMINAR EL TIPO_DE_DATO_2 Y/O DATOS_CLAVE_2'),
    --(22,'VALORES INVÁLIDOS, COMPLETAR O ELIMINAR EL TIPO_DE_DATO_3 Y/O DATOS_CLAVE_3')
    --(23,'Validar Control de Limite de Proceso')

    -------------------------------------------------------
    -- VALIDACION DE FILAS REPETIDAS EN EL ARCHIVO EXCEL --
    -------------------------------------------------------
    insert into #T_tabla_error
    select
        a.idtabla,
        a.nro_linea,
        a.linea,
        2
    from @I_Tabla a
        inner join (
            select b.linea, cant = count(1)
            from @I_Tabla b
            where b.idTabla = @F_id_tabla
                and b.nro_linea >= 2
            group by b.linea
        ) t
            on t.linea = a.linea
                and t.cant > 1
    where a.idTabla = @F_id_tabla
        and a.nro_linea >= 2
    order by a.nro_linea

    ---------------------------------------
    -- VALIDACIONES DE DATOS DEL ARCHIVO --
    ---------------------------------------

    ---------------------------------------------------------------------------
    -- validacion que el numero de notificacion(campo 1) sea entero positivo --
    ---------------------------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 1,
        cod_error   = 1
    from #t_datos_archivo as a
    where a.cod_campo = 1 --NRO_NOTIFICACION
        and (isnumeric(a.valor_campo) = 0 or cast(a.valor_campo as int) < 0)

    -------------------------------------------------------------------------
    -- validacion que el numero de solicitud (campo 2) sea entero positivo --
    -------------------------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 2,
        cod_error   = 1
    from #t_datos_archivo as a
    where a.cod_campo = 2 --NRO_SOLICITUD
        and (isnumeric(a.valor_campo) = 0 or cast(a.valor_campo as int) < 0)

    ---------------------------------------------
    -- validacion que el ENTE (campo 3) exista --
    ---------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 3,
        cod_error   = 10
    from #t_datos_archivo as a
    where a.cod_campo = 3 --ENTE
        and not exists (select c.sigla
                    from pamnot_ente c
                    where c.indicador = 'A'
                        and c.sigla = a.valor_campo
                        and c.fecha_proceso_hasta = '01-01-2050')

    -------------------------------------------------------------------
    -- validacion que el ENTE (campo 3) sea unico en todo el archivo --
    -------------------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 3,
        cod_error   = 3
    from #t_datos_archivo as a
    where a.cod_campo = 3 --ENTE
        and a.valor_campo <> @F_ente

    ------------------------------------------------------
    -- validacion que la FECHA (campo 4) sea tipo fecha --
    ------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 4,
        cod_error   = 17
    from #t_datos_archivo as a
    where a.cod_campo = 4 --FECHA
        and isdate(a.valor_campo) = 0

    ----------------------------------------------------------------------
    -- validacion que la FECHA (campo 4) sea unica en todo el docuemnto --
    ----------------------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 4,
        cod_error   = 3
    from #t_datos_archivo as a
    where a.cod_campo = 4 --FECHA
        and a.valor_campo <> @F_fecha

    ----------------------------------------------------------------------
    -- validacion que CIRCULAR (campo 5) sea unica en todo el docuemnto --
    ----------------------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 5,
        cod_error   = 3
    from #t_datos_archivo as a
    where a.cod_campo = 5 --CIRCULAR
        and a.valor_campo <> @F_codigo_notificacion

    -----------------------------------------------------------------------------------
    -- validacion que CIRCULAR (campo 5)  NO SE ENCUENTRE REGISTRADA EN NOTMST_CARGA --
    -----------------------------------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 5,
        cod_error   = 6
    from #t_datos_archivo as a
    where a.cod_campo = 5 --CIRCULAR
        and a.sec_carga = 1
        and exists (
                select 1
                from notmst_carga as b
                where b.codigo_notificacion = a.valor_campo
                    and b.indicador = 'A'
                    and b.fecha_proceso_hasta = '01-01-2050'
                    and b.estado = 1
            )

    ---------------------------------------------------------------------------------------------------------
    -- validar S/N (campo 6) tipo de solicitud debe ser  1=RETENCIONES O 2= LIBERACION Y DEBE SER POSITIVO --
    ---------------------------------------------------------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 6,
        cod_error   = 16
    from #t_datos_archivo as a
    where a.cod_campo = 6 --S/N
        and a.valor_campo not in ('1', '2')

    ---------------------------------------------------------
    -- validar que el tipo de solicitud(campo 6) sea unica --
    ---------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 6,
        cod_error   = 3
    from #t_datos_archivo as a
    where a.cod_campo = 6 --S/N
        and a.valor_campo <> @F_tipo_solicitud

    ----------------------------------------------------------
    -- validar TIPO_DATO_CLAVE1 (campo 7) no debe ser vacio --
    ----------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 7,
        cod_error   = 18
    from #t_datos_archivo as a
    where a.cod_campo = 7 --TIPO_DATO_CLAVE1
        and len(a.valor_campo) = 0

    -------------------------------------------------------------------------
    -- validar que el campo 7 TIPO_DATO_CLAVE1 exista en pamnot_dato_clave --
    -------------------------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 7,
        cod_error   = 8
    from #t_datos_archivo as a
    where a.cod_campo = 7 --TIPO_DATO_CLAVE1
        and not exists (
            select b.sigla
            from pamnot_dato_clave as b
            where  b.indicador = 'A'
                and b.sigla = a.valor_campo
                and b.fecha_proceso_hasta = '01-01-2050'
        )

    -----------------------------------------------------
    -- validar DATO_CLAVE1 (campo 8) no debe ser vacio --
    -----------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 8,
        cod_error   = 18
    from #t_datos_archivo as a
    where a.cod_campo = 8 --DATO_CLAVE1
        and len(a.valor_campo) = 0

    ---------------------------------------------------
    -- validar AUTORIDAD (campo 9) no debe ser vacio --
    ---------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 9,
        cod_error   = 18
    from #t_datos_archivo as a
    where a.cod_campo = 9 --AUTORIDAD
        and len(a.valor_campo) = 0

    --------------------------------------------------
    -- validar OFICINA (campo 10) no debe ser vacio --
    --------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 10,
        cod_error   = 18
    from #t_datos_archivo as a
    where a.cod_campo = 10 --OFICINA
        and len(a.valor_campo) = 0

    ------------------------------------------------
    -- validar CARGO (campo 11) no debe ser vacio --
    ------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 11,
        cod_error   = 18
    from #t_datos_archivo as a
    where a.cod_campo = 11 --CARGO
        and len(a.valor_campo) = 0

    --------------------------------------------------------------------
    -- valida que sea juridico o persona natural (campo 12,13,14,15 ) --
    --------------------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 15,
        cod_error   = 7
    from (
        select
            a.sec_carga,
            len_natural  = sum(iif(a.cod_campo in (12, 13, 14), LEN(a.valor_campo), 0)),
            len_juridico = sum(iif(a.cod_campo = 15, len(a.valor_campo), 0))
        from #t_datos_archivo as a
        where a.cod_campo in (12, 13, 14, 15) --AP. PATERNO, AP. MATERNO, NOMBRE, JURIDICO
        group by a.sec_carga  --por que group by ?
    ) as a
    where (len_natural = 0 and len_juridico = 0) or (len_natural > 0 and len_juridico > 0)

    ---------------------------------------------------------------------------
    -- validar tipo identificacion (campo 16)  exista en pam_identificacion  --
    ---------------------------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 16,
        cod_error   = 11
    from #t_datos_archivo as a
    where a.cod_campo = 16 --TIPO INDENTIFICACION
        and not exists (
            select b.sigla
            from pam_identificacion as b
            where b.indicador = 'A'
                and a.valor_campo = b.sigla
                and b.estado = 'A'
        )

    --------------------------------------------------------------------------------
    -- validar NIT/CI (campo 17) que sea numero y positivo si es persona juridica --
    --------------------------------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 17,
        cod_error   = 1
    from (
        select
            a.sec_carga,
            es_numerico   = sum(iif(a.cod_campo = 17, isnumeric(a.valor_campo), 0)),
            len_extencion = sum(iif(a.cod_campo = 18, len(a.valor_campo), 0)),
            sig_nit       = sum(iif(a.cod_campo = 17 and  isnumeric(a.valor_campo) = 1, SIGN(a.valor_campo), 0))
        from #t_datos_archivo as a
        where a.cod_campo in (17, 18) --NIT/C.I., extencion
        group by a.sec_carga
    ) as a
    where len_extencion > 0 and es_numerico = 0 and sig_nit = -1

    ------------------------------------------------------------------------
    -- validar EXTENCION (campo 18) solo debe tener si es persona natural --
    ------------------------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 18,
        cod_error   = 20
    from (
        select
            a.sec_carga,
            len_juridico  = sum(iif(a.cod_campo = 15, len(a.valor_campo), 0)),
            len_extencion = sum(iif(a.cod_campo = 18, len(a.valor_campo), 0))
        from #t_datos_archivo as a
        where a.cod_campo in (15, 18) --Juridico, extencion
        group by a.sec_carga
    ) as a
    where (len_juridico = 0 and len_extencion = 0)

    -------------------------------------------------------------------
    -- validar tipo de persona (campo 19) debe ser positivo numerico --
    -------------------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 19,
        cod_error   = 1
    from #t_datos_archivo as a
    where a.cod_campo = 19 --TIPO_PERSONA
        and (isnumeric(a.valor_campo) = 0
            or SIGN(a.valor_campo) = -1)

   ------------------------------------------------------------------------------------------
    -- validar si el tipo persona (campo 19) si es 1 = juridico no deveria tener extencion --
    -----------------------------------------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 19,
        cod_error   = 23
    from (
        select
            a.sec_carga,
            len_ci        = sum(iif(a.cod_campo = 18, LEN(ltrim(a.valor_campo)), 0)),
            tipo_per      = sum(iif(a.cod_campo = 19, a.valor_campo,0))
        from #t_datos_archivo as a
        where a.cod_campo in (18, 19) --Juridico, extencion
        group by a.sec_carga
    ) as a
    where (len_ci > 0 and tipo_per = 1)

    -------------------------------
    -- validar moneda (campo 20) --
    -------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 20,
        cod_error   = 9
    from #t_datos_archivo as a
    where a.cod_campo = 20 --MONEDA
    and not exists (select b.sigla
                    from pam_moneda as b
                    where b.indicador = 'A'
                        and a.valor_campo = b.sigla)

    -------------------------------------------------------
    -- validar monto (campo 21) debe ser entero positivo --
    -------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 21,
        cod_error   = 1
    from #t_datos_archivo as a
    where a.cod_campo = 21 --MONTO
        and (isnumeric(a.valor_campo) = 0
            or SIGN(a.valor_campo) = -1)

    -----------------------------------------------------------
    -- validar monto ufv (campo 22) debe ser entero positivo --
    -----------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 22,
        cod_error   = 1
    from #t_datos_archivo as a
    where a.cod_campo = 22 --MONTO_UFV
        and (isnumeric(a.valor_campo) = 0
            or SIGN(a.valor_campo) = -1)

    ------------------------------------------------------------------------------------------------
    -- validar TIPO_DATO_CLAVE2 (campo 23) debe tener valor si dato_clave2 (campo 24) tiene valor --
    ------------------------------------------------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 23,
        cod_error   = 21
   from (
        select
            a.sec_carga,
            len_tipo_dato_clave2     = sum(iif(a.cod_campo = 23, LEN(a.valor_campo), 0)),
            len_dato_clave2          = sum(iif(a.cod_campo = 24, len(a.valor_campo), 0))
        from #t_datos_archivo as a
        where a.cod_campo in (23, 24) --TIPO_DATO_CLAVE2, DATO_CLAVE2
        group by a.sec_carga
    ) as a
    where (len_tipo_dato_clave2 = 0 and len_dato_clave2 > 0)
        or (len_tipo_dato_clave2 > 0 and len_dato_clave2 = 0)

    ---------------------------------------------------------------------
    -- validar TIPO_DATO_CLAVE2 (campo 23) exista en pamnot_dato_clave --
    ---------------------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 23,
        cod_error   = 8
    from #t_datos_archivo as a
    where a.cod_campo = 23 --TIPO_DATO_CLAVE2
        and a.valor_campo <> ''
        and not exists (select b.sigla
                            from pamnot_dato_clave b
                            where indicador = 'A'
                                and fecha_proceso_hasta = '01-01-2050'
                                and a.valor_campo = b.sigla)

    ------------------------------------------------------
    -- validar S/N2 (campo 25) DEBE SER ENTERO POSITIVO --
    ------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 25,
        cod_error   = 1
    from #t_datos_archivo as a
    where a.cod_campo = 25 --S/N2
    and (isnumeric(a.valor_campo) = 0
        or SIGN(a.valor_campo) = -1)

    ------------------------------------------------------------------------------------------------
    -- validar TIPO_DATO_CLAVE3 (campo 27) debe tener valor si dato_clave3 (campo 28) tiene valor --
    ------------------------------------------------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 27,
        cod_error   = 22
    from (
        select
            a.sec_carga,
            len_tipo_dato_clave3     = sum(iif(a.cod_campo = 27, len(a.valor_campo), 0)),
            len_dato_clave3          = sum(iif(a.cod_campo = 28, len(a.valor_campo), 0))
        from #t_datos_archivo as a
        where a.cod_campo in (27, 28) --TIPO_DATO_CLAVE3, DATO_CLAVE3
        group by a.sec_carga
    ) as a
    where (len_tipo_dato_clave3 = 0 and len_dato_clave3 > 0)
        or (len_tipo_dato_clave3 > 0 and len_dato_clave3 = 0)

    ---------------------------------------------------------------------
    -- validar TIPO_DATO_CLAVE3 (campo 27) exista en pamnot_dato_clave --
    ---------------------------------------------------------------------
    insert into #T_error
    select
        fila        = a.sec_carga,
        nro_campo   = 27,
        cod_error   = 8
   from #t_datos_archivo as a
    where a.cod_campo = 27 --TIPO_DATO_CLAVE3
        and a.valor_campo <> ''
        and not exists (select b.sigla
                            from pamnot_dato_clave b
                            where indicador = 'A'
                                and fecha_proceso_hasta = '01-01-2050'
                                and a.valor_campo = b.sigla)

    --MOSTRAR REPORTE DE VALIDACIONES Y HACER ROLLBACK
    if exists (select 1 from #T_error where cod_error <> 0) or exists (select 1 from #T_tabla_error where cod_error <> 0)
    begin
        set @O_error_msg = 'EXISTE ERROR DE VALIDACIÓN EN LA TABLA.'
        goto LINEA_ERROR
    end

    ------------------------------------------------------------------------
    -- CODIGO SOLICITUD: R-1 R-2 R-3 ... (agrupacion por campo AUTORIDAD) --
    ------------------------------------------------------------------------
    ;with
        tmp_codigo_solicitud as (
            SELECT
                cod_campo,
                valor_campo,
                sec_r_sol = ROW_NUMBER() OVER (ORDER BY sec_carga)
            FROM (
                SELECT
                    cod_campo,
                    valor_campo,
                    sec_carga = min(sec_carga)
                FROM #t_datos_archivo
                where cod_campo = 9 --AUTORIDAD
                group by valor_campo, cod_campo
            ) as b
        )
    update c
    set codigo_solicitud = 'R-' + cast(b.sec_r_sol as varchar(10))
    from tmp_codigo_solicitud b
        inner join #t_datos_archivo a
            on a.cod_campo = b.cod_campo
                and a.valor_campo = b.valor_campo
        inner join #t_datos_archivo c
            on a.sec_carga = c.sec_carga

    ----------------------------------------------
    -- OBTENIENDO EL CORRELATIVO PARA NRO_CARGA --
    ----------------------------------------------
    if @F_nuevo = 1
    begin
        exec @F_error_exec = dbo.proc_not_leer_y_act_pamcorrelativo
            @I_tipo_correlativo  = 1,                -- 1=Notificación-Solicitud, 2=Comprobante de orden y tran
            @I_fecha_proceso     = @F_fecha_proceso,
            @O_correlativo       = @F_nro_carga  OUTPUT,
            @O_error_msg         = @O_error_msg  OUTPUT

        if @@error <> 0
        begin
            set @O_error_msg = 'Error en ejecutar proc_not_leer_y_act_pamcorrelativo.'
            goto linea_error
        end

        if @F_error_exec <> 0
            goto linea_error
    end

    ----------------------------------------
    -- CABECERA  -> TABLA :  NOTMST_CARGA --
    ----------------------------------------
    exec @F_error_exec = dbo.proc_not_insert_update_notmst_carga
        @I_reversion             =  0, -- 0 grabar ,1 update
        -------------------------------------------
        @I_nro_carga             =  @F_nro_carga ,
        @I_nro_notificacion      =  0,
        @I_codigo_notificacion   =  @F_codigo_notificacion,
        @I_tipo_solicitud        =  @F_tipo_solicitud,
        @I_cod_tabla             =  @F_id_tabla,
        @I_estado                =  0,
        @I_usuario               =  @F_usuario,
        @I_fecha                 =  @F_fecha,
        @I_observacion           =  '',
        @I_fecha_proceso         =  @F_fecha_proceso,
        @I_fecha_proceso_hasta   =  @F_fecha_proceso_hasta,
        -------------------------------------------
        @O_error_msg             =  @O_error_msg OUTPUT

    if @@error <> 0
    begin
        set @O_error_msg = 'Error en ejecutar proc_not_insert_update_notmst_carga.'
        goto linea_error
    end

    if @F_error_exec <> 0
        goto linea_error

    -----------------------------------------------------
    -- INSERTAR DATOS EN TABLA -> NOTMST_CARGA_DETALLE --
    -----------------------------------------------------
    insert into notmst_carga_detalle
    select
        codigo_notificacion = a.codigo_notificacion,     -- CIRCULAR
        codigo_solicitud    = a.codigo_solicitud, -- R-1...
        nro_carga           = @F_nro_carga,       -- pam_correlativo()
        sec_carga           = a.sec_carga,        -- numero de fila
        cod_campo           = a.cod_campo,        -- incremetal hasta 1 -> 32
        valor_campo         = a.valor_campo,
        fecha_proceso       = @F_fecha_proceso,
        fecha_proceso_hasta = '01-01-2050',
        indicador           = 'A',
        sec                 = 1
    from #t_datos_archivo a
    order by a.sec_carga, a.cod_campo
END

salir_procedimiento:
    if (@@NESTLEVEL = 1) OR (@@NESTLEVEL > 1 AND @F_id_tabla > 0)
    begin
        if @I_grabar = 1 and @@TRANCOUNT > 0
        begin
            commit tran

            select linea_texto = 'M' + CHAR(13) + 'Nro. Carga: ' + CAST(@F_nro_carga as varchar(12))
        end

        set @F_observacion_error = 'Grab:' + isnull(cast(@I_grabar as varchar(1)), 'Nulo') +
                                'Arch:' + isnull(@I_nombre_archivo, 'Nulo')

        exec dbo.Grabar_log_GLB
            @I_usuario            = @F_usuario,
            @I_fecha_proceso      = @F_fecha_proceso,
            @I_observacion_error  = @F_observacion_error,
            @I_nombre_sp          = 'proc_not_carga_archivo_solicitud',  --Nombre del procedimiento almacenado
            @I_fecha_desde        = @F_fecha_desde_log,
            @I_tipo_log           = 2,                                             --1=reporte, 2=abm
            @I_es_error           = 0,                                             --0=no es error, 1=si es error
            @I_error_msg          = ''
    end

    RETURN 0

linea_error:
    if @F_ctrl_ejecutado = 0
    begin
        select linea_texto = 'R', 'Listado de errores de validación.'

        exec dbo.proc_repnot_validar_carga
    end

    set @O_error_msg = 'proc_not_carga_archivo_solicitud.§' + ISNULL(@O_error_msg, 'Error Nulo')

    if @@NESTLEVEL >= 1
    BEGIN
        if (@@NESTLEVEL = 1) OR (@@NESTLEVEL > 1 AND @F_id_tabla > 0)
        begin
            if @I_grabar = 1 and @@trancount > 0
                rollback tran

            set @F_observacion_error = 'Grab:' + isnull(cast(@I_grabar as varchar(1)),'Nulo')+
                                    'Arch:' + isnull(@I_nombre_archivo, 'Nulo')

            exec dbo.Grabar_log_GLB
                @I_usuario            = @F_usuario,
                @I_fecha_proceso      = @F_fecha_proceso,
                @I_observacion_error  = @F_observacion_error,
                @I_nombre_sp          = 'proc_not_carga_archivo_solicitud',  --Nombre del procedimiento almacenado
                @I_fecha_desde        = @F_fecha_desde_log,
                @I_tipo_log           = 2,                                             --1=reporte, 2=abm
                @I_es_error           = 1,                                             --0=no es error, 1=si es error
                @I_error_msg          = @O_error_msg
        END
    END

    if @F_ctrl_ejecutado = 1
        and ((@@NESTLEVEL = 1) OR (@@NESTLEVEL > 1 AND @F_id_tabla > 0))
    begin
        RAISERROR(@O_error_msg, 16, -1)
    end

    RETURN -1
GO
