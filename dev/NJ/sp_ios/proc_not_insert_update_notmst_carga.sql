﻿SET ANSI_NULLS, QUOTED_IDENTIFIER ON
GO
CREATE or alter PROCEDURE dbo.proc_not_insert_update_notmst_carga
    @I_reversion             tinyint       = 0 ,
    -------------------------------------------
    @I_nro_carga             decimal(12)   = 0 ,
    @I_nro_notificacion      decimal(12)   = 0 ,
    @I_codigo_notificacion   varchar(40)   = '',
    @I_tipo_solicitud        tinyint       = 0 ,
    @I_cod_tabla             int           = 0 ,
    @I_estado                tinyint       = 0 ,
    @I_usuario               int           = 0 ,
    @I_fecha                 datetime      = '',
    @I_observacion           varchar(300)  = '',
    @I_obs_desh              varchar(200)  = '',
    @I_fecha_proceso         smalldatetime = '',
    @I_fecha_proceso_hasta   smalldatetime = '',
    -------------------------------------------
    @O_error_msg             varchar(300)  = '' OUTPUT
WITH ENCRYPTION
as
/*************************************************************************************************/
/* DESCRIPCION: registro/modificación de la carga masiva de archivos,                            */
/*              datos masivos que se identifican por un código de notificación único por carga   */
/* OBSERVACION: RD-8837 Pase IV, correcion en la reversion y generacion de sec                   */
/* REV.CALIDAD: OK                                                                               */
/*************************************************************************************************/
declare
    @F_nro_carga             decimal(12)    = 0,
    @F_nro_notificacion      decimal(12)    = 0,
    @F_codigo_notificacion   varchar(40)    = '',
    @F_tipo_solicitud        tinyint        = 0,
    @F_cod_tabla             int            = 0,
    @F_estado                tinyint        = 0,
    @F_usuario               int            = 0,
    @F_fecha                 datetime       = '',
    @F_observacion           varchar(300)   = '',
    --
    @F_indicador             char(1)        = '',
    @F_sec                   smallint       = 0,
    @F_usuario_desh          int            = 0,
    @F_fecha_desh            smalldatetime  = '',
    @F_obs_desh              varchar(200)   = '',
    @F_fecha_today           smalldatetime  = '',
    --
    @F_error                int,
    @F_rowcount             int

set @F_fecha_today  = GETDATE()

select
    @F_nro_carga           = Nro_carga,
    @F_nro_notificacion    = nro_notificacion,
    @F_codigo_notificacion = codigo_notificacion ,
    @F_tipo_solicitud      = tipo_solicitud,
    @F_cod_tabla           = cod_tabla,
    @F_estado              = estado,
    @F_usuario             = usuario,
    @F_fecha               = fecha,
    @F_observacion         = observacion
from notmst_carga
where nro_carga = @I_nro_carga
    and indicador = 'A'
    and fecha_proceso_hasta = '01-01-2050'

select @F_error = @@ERROR, @F_rowcount = @@ROWCOUNT

if @F_error = 0 and @F_rowcount = 1
begin
    if @I_nro_carga           is null set  @I_nro_carga           = @F_nro_carga
    if @I_nro_notificacion    is null set  @I_nro_notificacion    = @F_nro_notificacion
    if @I_codigo_notificacion is null set  @I_codigo_notificacion = @F_codigo_notificacion
    if @I_tipo_solicitud      is null set  @I_tipo_solicitud      = @F_tipo_solicitud
    if @I_cod_tabla           is null set  @I_cod_tabla           = @F_cod_tabla
    if @I_estado              is null set  @I_estado              = @F_estado
    if @I_usuario             is null set  @I_usuario             = @F_usuario
    if @I_fecha               is null set  @I_fecha               = @F_fecha
    if @I_observacion         is null set  @I_observacion         = @F_observacion
end
--------------------------------------------------------------------
---               VALIDAR DATOS                                  ---
--------------------------------------------------------------------
if @I_reversion = 2
begin
    if LEN(@I_obs_desh) = 0
    begin
        set @O_error_msg = 'Error: debe registrar una observación motivo del Rechazo de la carga.'
        goto linea_error
    end

    --VERIFICAR QUE NO SE ENCUENTRE PROCESADA, PARA CAMBIAR ESTADO A RECHAZADO
    if @F_rowcount = 1
        and exists (select 1
                    from notmst_notificacion c
                    where c.codigo_notificacion = @I_codigo_notificacion
                            and c.fecha_proceso_hasta = '01-01-2050'
                            and c.indicador = 'A')
    begin
        set @O_error_msg = 'No se puede actualizar el Estado: Rechazada, la carga del archivo ya se encuentra procesada. Favor Verifique.'
        goto linea_error
    end
end
--------------------------------------------------------------------
---               REGISTRAR DATOS                                ---
--------------------------------------------------------------------
if @I_reversion = 0
begin
    if @F_rowcount = 1
    begin
        update notmst_carga
        set indicador           = iif(fecha_proceso = @I_fecha_proceso, 'N', 'A'),
            fecha_proceso_hasta = iif(fecha_proceso = @I_fecha_proceso, fecha_proceso, @I_fecha_proceso - 1),
            @F_sec              = iif(fecha_proceso = @I_fecha_proceso, sec + 1, 1)
        from notmst_carga
        where nro_carga = @I_nro_carga
            and indicador = 'A'
            and fecha_proceso_hasta = '01-01-2050'

        if @@error <> 0
        begin
            set @O_error_msg = 'Error al actualizar notmst_carga'
            goto linea_error
        end
    end
    else
    begin
        set @F_sec = 1
    end

    insert into notmst_carga
    select
        nro_carga             = @I_nro_carga,
        nro_notificacion      = @I_nro_notificacion,
        codigo_notificacion   = @I_codigo_notificacion,
        tipo_solicitud        = @I_tipo_solicitud,
        cod_tabla             = @I_cod_tabla,
        estado                = @I_estado,
        usuario               = @I_usuario,
        fecha                 = @I_fecha,
        observacion           = @I_observacion,
        fecha_proceso         = @I_fecha_proceso,
        fecha_proceso_hasta   = @I_fecha_proceso_hasta,
        indicador             = 'A',
        sec                   = @F_sec,
        usuario_desh          = @F_usuario_desh,
        fecha_desh            = @F_fecha_desh,
        obs_desh              = @F_obs_desh

    if @@error <> 0
    begin
        set @O_error_msg = 'Error al insertar en notmst_carga.'
        goto linea_error
    end
end
else -- REVERSIÓN
begin
    update notmst_carga
    set indicador           = 'B',
        fecha_proceso_hasta = @I_fecha_proceso,
        usuario_desh        = @I_usuario      ,
        fecha_desh          = @F_fecha_today  ,
        obs_desh            = @I_obs_desh
    where nro_carga = @I_nro_carga
        and indicador  = 'A'
        and fecha_proceso_hasta = '01-01-2050'

    if @@error <> 0
    begin
        set @O_error_msg = 'Error al actualizar notmst_carga.'
        goto linea_error
    end

    update notmst_carga_detalle
    set indicador = 'B',
        fecha_proceso_hasta = @I_fecha_proceso
    from notmst_carga_detalle
    where nro_carga = @I_nro_carga
        and indicador = 'A'
        and fecha_proceso_hasta = '01-01-2050'

    if @@error <> 0
    begin
        set @O_error_msg = 'Error al actualizar notmst_carga_detalle.'
        goto linea_error
    end
end

return 0

linea_error:
    set @O_error_msg = 'proc_not_insert_update_notmst_carga.§' + isnull(@O_error_msg, 'Nulo')

    if @@nestlevel = 1
        RAISERROR (@O_error_msg, 16, -1)

    return -1
GO
