﻿SET ANSI_NULLS, QUOTED_IDENTIFIER ON
GO
CREATE or alter PROCEDURE dbo.proc_abmnot_validar_notmst_carga
    @I_grabar         int = 0,
    @I_nro_carga      int = 0,
    @I_validar_carga  int = 0,
    @I_obs_desh       varchar(200) = '',
    @O_error_msg      varchar(600) = '' output
WITH ENCRYPTION
as
/**********************************************************************************************/
/* DESCRIPCIÓN: Programa que permite actualizar el estado de validación de la carga masiva    */
/*               de clientes demandados en la carta de notificación.                          */
/* OBSERVACIÓN: RD-8837 IV, mejoras en la validacion de la carga de archivos                  */
/* REV.CALIDAD: OK                                                                            */
/**********************************************************************************************/
set nocount on
set XACT_ABORT ON -- Notificamos al Motor que si ocurre algun error en la Transaccion lo Revierta.

if @@trancount > 0 and @@NESTLEVEL = 1 -- Si existe alguna Transaccion activa en esta Session lo Revertimos.
    rollback tran

if @@NESTLEVEL = 1
    select nombre_reporte = 'SinReporte'

if @@NESTLEVEL = 1 and @I_grabar = 1
    BEGIN TRAN

declare
    @F_error               smallint      = 0 ,
    @F_error_exec          smallint      = 0 ,
    @F_fecha_desde_log     datetime      = '01-01-1900',
    @F_observacion_error   varchar(200)  = '',
    @F_fecha_proceso_hasta smalldatetime = '01-01-1900',

    @F_tipo_baja            tinyint      = 0,

    @F_nro_notificacion     decimal(12)  = 0,
    @F_codigo_notificacion  varchar(40)  = '',
    @F_tipo_solicitud       tinyint      = 0,
    @F_cod_tabla            int          = 0,
    @F_estado               tinyint      = 0,
    @F_usuario              int          = 0,
    @F_fecha                datetime     = '',
    @F_observacion          varchar(300) = '',

    @F_usuario_s            int          = 0,
    @F_fecha_proceso_s      datetime     = ''

exec @F_error_exec = dbo.proc_validar_usuario_fecha_proceso
    @I_sistema               = 400,                    --400
    @O_usuario               = @F_usuario_s           output,
    @O_fecha_proceso         = @F_fecha_proceso_s     output,
    @O_fecha_proceso_hasta   = @F_fecha_proceso_hasta output,
    @O_error                 = @F_error               output,
    @O_error_msg             = @O_error_msg           output

if @F_error_exec <> 0
    goto linea_error

------------------------------
-- VALIDACIONES DE PANTALLA --
------------------------------
if @I_validar_carga <= 0
begin
    set @O_error_msg = 'Seleccione un estado de validacion.'
    goto linea_Error
end

select
    @F_nro_notificacion     = nro_notificacion,
    @F_codigo_notificacion  = codigo_notificacion,
    @F_tipo_solicitud       = tipo_solicitud,
    @F_cod_tabla            = cod_tabla,
    @F_estado               = estado    ,
    @F_usuario              = usuario,
    @F_fecha                = fecha,
    @F_observacion          = observacion
from notmst_carga
where nro_carga = @I_nro_carga
    and indicador = 'A'
    and fecha_proceso_hasta = '01-01-2050'

if @@ROWCOUNT = 0
begin
    set @O_error_msg = 'No se puede modificar el estado de validación, el NRO Carga no existe. Favor verifique.'
    goto linea_Error
end

if @I_validar_carga = 1
    and exists (
            select 1
            from notmst_carga
            where fecha_proceso_hasta = '01-01-2050'
                and indicador = 'A'
                and estado = 1
                and codigo_notificacion = @F_codigo_notificacion
        )
begin
    set @O_error_msg = 'Ya existe una carga con la CIRCULAR ' + @F_codigo_notificacion + ', no se puede aceptar. Favor verifique.'
    goto linea_Error
end

if @I_grabar = 0
    and @F_tipo_baja = 0
    and @F_estado = @I_validar_carga
    and @F_observacion = @I_obs_desh
begin
    set @O_error_msg = 'No existen modificaciones. Por favor verifique...'
    goto linea_Error
end

if @I_grabar = 1
begin
    set @F_observacion = @I_obs_desh

    if @I_validar_carga = 2 --Rechazado
        set @F_tipo_baja = 2 --Baja

    exec @F_error_exec = dbo.proc_not_insert_update_notmst_carga
        @I_reversion             = @F_tipo_baja, -- 0 grabar, 1 update 2 baja
        -------------------------------------------
        @I_nro_carga             = @I_nro_carga,
        @I_nro_notificacion      = @F_nro_notificacion,
        @I_codigo_notificacion   = @F_codigo_notificacion,
        @I_tipo_solicitud        = @F_tipo_solicitud,
        @I_cod_tabla             = @F_cod_tabla,
        @I_estado                = @I_validar_carga,
        @I_usuario               = @F_usuario_s,
        @I_fecha                 = @F_fecha,
        @I_observacion           = @F_observacion,
        @I_obs_desh              = @I_obs_desh,
        @I_fecha_proceso         = @F_fecha_proceso_s,
        @I_fecha_proceso_hasta   = @F_fecha_proceso_hasta,
        -------------------------------------------
        @O_error_msg             = @O_error_msg OUTPUT

    if @@error <> 0
    begin
        set @O_error_msg = 'Error en ejecutar proc_not_insert_update_notmst_carga'
        goto linea_error
    end

    if @F_error_exec <> 0
        goto linea_error
end

salir_procedimiento:
    if  @@NESTLEVEL = 1 and @I_grabar = 1
    begin
        commit tran

        select linea_texto = 'M' + 'TRANSACCION OK!. ' + char(13) + isnull('', 'NULO')

        select linea_texto = 'R', 'Listado de datos de la carga'

        exec dbo.proc_repnot_carga_archivo
            @I_nro_carga    = @I_nro_carga,
            @I_fecha        = @F_fecha_proceso_s,
            @I_ascii        = 2, --1. SI, 2. NO
            @O_error_msg    = @O_error_msg output


        set @F_observacion_error = 'Grab:' + isnull(cast(@I_grabar as varchar(2)), 'Nulo') + ',' +
                                'Nro_carga:'+ isnull(cast(@I_nro_carga as varchar(12)), 'Nulo')

        exec dbo.Grabar_log_GLB
            @I_usuario            = @F_usuario_s,
            @I_fecha_proceso      = @F_fecha_proceso_s,
            @I_observacion_error  = @F_observacion_error,
            @I_nombre_sp          = 'proc_abmnot_validar_notmst_carga', --Nombre del procedimiento almacenado
            @I_fecha_desde        = @F_fecha_desde_log,
            @I_tipo_log           = 2, --1=reporte, 2=abm
            @I_es_error           = 0, --0=no es error, 1=si es error
            @I_error_msg          = ''
    end

    RETURN 0

linea_error:
    set @O_error_msg = 'proc_abmnot_validar_notmst_carga.§' + ISNULL(@O_error_msg, 'Error Nulo')

    if @@NESTLEVEL = 1
    BEGIN
        if @@trancount > 0 AND @I_grabar = 1
            rollback tran

        set @F_observacion_error = 'Grab:' + isnull(cast(@I_grabar as varchar(2)), 'Nulo') + ',' +
                                  'Nro_carga:'+ isnull(cast(@I_nro_carga as varchar(12)), 'Nulo')

        exec dbo.Grabar_log_GLB
            @I_usuario            = @F_usuario_s,
            @I_fecha_proceso      = @F_fecha_proceso_s,
            @I_observacion_error  = @F_observacion_error,
            @I_nombre_sp          = 'proc_abmnot_validar_notmst_carga', --Nombre del procedimiento almacenado
            @I_fecha_desde        = @F_fecha_desde_log,
            @I_tipo_log           = 2, --1=reporte, 2=abm
            @I_es_error           = 1, --0=no es error, 1=si es error
            @I_error_msg          = @O_error_msg
    END

    if @@nestlevel = 1
        RAISERROR(@O_error_msg, 16, -1)

    RETURN -1
GO
