﻿SET ANSI_NULLS, QUOTED_IDENTIFIER ON
GO
create or alter procedure dbo.proc_repnot_carga_archivo
    @I_nro_carga      int      = 0 ,    --nro_carga del archivo
    @I_fecha          datetime = '',    --Fecha de corte
    @I_ascii          tinyint  = 0 ,    --1:SI, 2=NO
    -----------------------------------------
    @O_error_msg      varchar(600) = '' output
WITH ENCRYPTION
as
/****************************************************************************/
/* DESCRIPCIÓN: Reporte para visualizar datos de la carga de archivo        */
/* OBSERVACIÓN: RD-8837 IV, identacion de codigo                            */
/* REV.CALIDAD: OK                                                          */
/****************************************************************************/
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

declare
    @F_error                int           = 0 ,
    @F_usuario              int           = 0 ,
    @F_obs_error            varchar(200)  = '',
    @F_error_exec           int           = 0 ,
    @F_fecha_today          datetime      = '01-01-1900',
    @F_fecha_proceso        smalldatetime = '01-01-1900',
    @F_fecha_desde_log      datetime      = '01-01-1900',
    @F_fecha_proceso_hasta  smalldatetime = '01-01-1900',

    @F_id_tabla             int           = 97,

    @F_titulo               varchar(1000) = '',
    @F_cabecera             varchar(1000) = '',
    @F_ancho_reporte        smallint      = 0 ,
    @F_lineas_cabecera      varchar(1000) = ''

create table #T_cabecera (
    campo1  varchar(300),   campo2  varchar(300),   campo3  varchar(300),   campo4  varchar(300),   campo5  varchar(300),
    campo6  varchar(300),   campo7  varchar(300),   campo8  varchar(300),   campo9  varchar(300),   campo10 varchar(300),

    campo11 varchar(300),   campo12 varchar(300),   campo13 varchar(300),   campo14 varchar(300),   campo15 varchar(300),
    campo16 varchar(300),   campo17 varchar(300),   campo18 varchar(300),   campo19 varchar(300),   campo20 varchar(300),

    campo21 varchar(300),   campo22 varchar(300),   campo23 varchar(300),   campo24 varchar(300),   campo25 varchar(300),
    campo26 varchar(300),   campo27 varchar(300),   campo28 varchar(300),   campo29 varchar(300),   campo30 varchar(300),

    campo31 varchar(300),   campo32 varchar(300),   campo33 varchar(300),   campo34 varchar(300),   campo35 varchar(300),
    campo36 varchar(300),   campo37 varchar(300),   campo38 varchar(300),   campo39 varchar(300),   campo40 varchar(300),

    campo41 varchar(300),   campo42 varchar(300),   campo43 varchar(300),   campo44 varchar(300),   campo45 varchar(300),
    campo46 varchar(300),   campo47 varchar(300),   campo48 varchar(300),   campo49 varchar(300),   campo50 varchar(300)
)

create table #T_contenido (
    secuencia           int identity(1,1),
    codigo_solicitud    varchar(300),

    campo1  varchar(300),   campo2  varchar(300),   campo3  varchar(300),   campo4  varchar(300),   campo5  varchar(300),
    campo6  varchar(300),   campo7  varchar(300),   campo8  varchar(300),   campo9  varchar(300),   campo10 varchar(300),

    campo11 varchar(300),   campo12 varchar(300),   campo13 varchar(300),   campo14 varchar(300),   campo15 varchar(300),
    campo16 varchar(300),   campo17 varchar(300),   campo18 varchar(300),   campo19 varchar(300),   campo20 varchar(300),

    campo21 varchar(300),   campo22 varchar(300),   campo23 varchar(300),   campo24 varchar(300),   campo25 varchar(300),
    campo26 varchar(300),   campo27 varchar(300),   campo28 varchar(300),   campo29 varchar(300),   campo30 varchar(300),

    campo31 varchar(300),   campo32 varchar(300),   campo33 varchar(300),   campo34 varchar(300),   campo35 varchar(300),
    campo36 varchar(300),   campo37 varchar(300),   campo38 varchar(300),   campo39 varchar(300),   campo40 varchar(300),

    campo41 varchar(300),   campo42 varchar(300),   campo43 varchar(300),   campo44 varchar(300),   campo45 varchar(300),
    campo46 varchar(300),   campo47 varchar(300),   campo48 varchar(300),   campo49 varchar(300),   campo50 varchar(300)
)

set @F_fecha_desde_log = getdate()

exec @F_error_exec = dbo.proc_validar_usuario_fecha_proceso
    @I_sistema               = 400,                   --
    @O_usuario               = @F_usuario             output,
    @O_fecha_proceso         = @F_fecha_proceso       output,
    @O_fecha_today           = @F_fecha_today         output,
    @O_fecha_proceso_hasta   = @F_fecha_proceso_hasta output,
    @O_error                 = @F_error               output,
    @O_error_msg             = @O_error_msg           output

if @F_error_exec <> 0
    goto linea_error

----------------------------
-- VALIDACION DE PANTALLA --
----------------------------
if @I_ascii = 0
    set @I_ascii = 2

----------------------------------------------------
-- OBTENER DATOS DE LA FUCION EN TABLAS VARIABLES --
----------------------------------------------------
insert into #T_cabecera
select
    campo1 , campo2 , campo3 , campo4 , campo5 , campo6 , campo7 , campo8 , campo9 , campo10,
    campo11, campo12, campo13, campo14, campo15, campo16, campo17, campo18, campo19, campo20,
    campo21, campo22, campo23, campo24, campo25, campo26, campo27, campo28, campo29, campo30,
    campo31, campo32, campo33, campo34, campo35, campo36, campo37, campo38, campo39, campo40,
    campo41, campo42, campo43, campo44, campo45, campo46, campo47, campo48, campo49, campo50
from dbo.fnt_not_obtener_tabla_cabecera(@F_id_tabla) --(97)

insert into #T_contenido
select
    codigo_solicitud,
    campo1 , campo2 , campo3 , campo4 , campo5 , campo6 , campo7 , campo8 , campo9 , campo10,
    campo11, campo12, campo13, campo14, campo15, campo16, campo17, campo18, campo19, campo20,
    campo21, campo22, campo23, campo24, campo25, campo26, campo27, campo28, campo29, campo30,
    campo31, campo32, campo33, campo34, campo35, campo36, campo37, campo38, campo39, campo40,
    campo41, campo42, campo43, campo44, campo45, campo46, campo47, campo48, campo49, campo50
from dbo.fnt_not_obtener_tabla_contenido(@I_nro_carga, @I_fecha)

----------------------------
-- ENCABEZADO DEL REPORTE --
----------------------------
if @I_ascii <> 1 -- NO ASCII
begin
    select @F_cabecera =
        cast(rtrim(isnull(+'NRO.',''))  as char(6))  +''+
        case when '' = campo1  then cast('' as char(16))  else cast(rtrim(isnull(+'|'+campo1,'')) as char(16)) end    +''+
        case when '' = campo2  then cast('' as char(14))  else cast(rtrim(isnull(+'|'+campo2,'')) as char(14)) end    +''+
        cast(rtrim(isnull(+'|COD_SOLICITUD.',''))  as char(14))  +''+
        case when '' = campo3  then cast('' as char(10))  else cast(rtrim(isnull(+'|'+campo3,'')) as char(10)) end    +''+
        case when '' = campo4  then cast('' as char(12))  else cast(rtrim(isnull(+'|'+campo4,'')) as char(12)) end    +''+
        case when '' = campo5  then cast('' as char(40))  else cast(rtrim(isnull(+'|'+campo5,'')) as char(40)) end    +''+
        case when '' = campo6  then cast('' as char(5 ))  else cast(rtrim(isnull(+'|'+campo6,'')) as char(5 )) end    +''+
        case when '' = campo7  then cast('' as char(16))  else cast(rtrim(isnull(+'|'+campo7,'')) as char(16)) end    +''+
        case when '' = campo8  then cast('' as char(50))  else cast(rtrim(isnull(+'|'+campo8,'')) as char(50)) end    +''+
        case when '' = campo9  then cast('' as char(40))  else cast(rtrim(isnull(+'|'+campo9,'')) as char(40)) end    +''+

        case when '' = campo10 then cast('' as char(40))  else cast(rtrim(isnull(+'|'+campo10,'')) as char(40))  end   +''+
        case when '' = campo11 then cast('' as char(40))  else cast(rtrim(isnull(+'|'+campo11,'')) as char(40))  end   +''+
        case when '' = campo12 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo12,'')) as char(20))  end   +''+
        case when '' = campo13 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo13,'')) as char(20))  end   +''+
        case when '' = campo14 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo14,'')) as char(20))  end   +''+
        case when '' = campo15 then cast('' as char(50))  else cast(rtrim(isnull(+'|'+campo15,'')) as char(50))  end   +''+
        case when '' = campo16 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo16,'')) as char(20))  end   +''+
        case when '' = campo17 then cast('' as char(15))  else cast(rtrim(isnull(+'|'+campo17,'')) as char(15))  end   +''+
        case when '' = campo18 then cast('' as char(10))  else cast(rtrim(isnull(+'|'+campo18,'')) as char(10))  end   +''+
        case when '' = campo19 then cast('' as char(12))  else cast(rtrim(isnull(+'|'+campo19,'')) as char(12))  end   +''+

        case when '' = campo20 then cast('' as char(7 ))  else cast(rtrim(isnull(+'|'+campo20,'')) as char(7 ))  end   +''+
        case when '' = campo21 then cast('' as char(12))  else cast(rtrim(isnull(+'|'+campo21,'')) as char(12))  end   +''+
        case when '' = campo22 then cast('' as char(12))  else cast(rtrim(isnull(+'|'+campo22,'')) as char(12))  end   +''+
        case when '' = campo23 then cast('' as char(16))  else cast(rtrim(isnull(+'|'+campo23,'')) as char(16))  end   +''+
        case when '' = campo24 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo24,'')) as char(20))  end   +''+
        case when '' = campo25 then cast('' as char(5 ))  else cast(rtrim(isnull(+'|'+campo25,'')) as char(5 ))  end   +''+
        case when '' = campo26 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo26,'')) as char(20))  end   +''+
        case when '' = campo27 then cast('' as char(16))  else cast(rtrim(isnull(+'|'+campo27,'')) as char(16))  end   +''+
        case when '' = campo28 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo28,'')) as char(20))  end   +''+
        case when '' = campo29 then cast('' as char(16))  else cast(rtrim(isnull(+'|'+campo29,'')) as char(16))  end   +''+

        case when '' = campo30 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo30,'')) as char(20))  end   +''+
        case when '' = campo31 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo31,'')) as char(20))  end   +''+
        case when '' = campo32 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo32,'')) as char(20))  end   +''+
        case when '' = campo33 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo33,'')) as char(20))  end   +''+
        case when '' = campo34 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo34,'')) as char(20))  end   +''+
        case when '' = campo35 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo35,'')) as char(20))  end   +''+
        case when '' = campo36 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo36,'')) as char(20))  end   +''+
        case when '' = campo37 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo37,'')) as char(20))  end   +''+
        case when '' = campo38 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo38,'')) as char(20))  end   +''+
        case when '' = campo39 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo39,'')) as char(20))  end   +''+

        case when '' = campo40 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo40,'')) as char(20))  end   +''+
        case when '' = campo41 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo41,'')) as char(20))  end   +''+
        case when '' = campo42 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo42,'')) as char(20))  end   +''+
        case when '' = campo43 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo43,'')) as char(20))  end   +''+
        case when '' = campo44 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo44,'')) as char(20))  end   +''+
        case when '' = campo45 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo45,'')) as char(20))  end   +''+
        case when '' = campo46 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo46,'')) as char(20))  end   +''+
        case when '' = campo47 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo47,'')) as char(20))  end   +''+
        case when '' = campo48 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo48,'')) as char(20))  end   +''+
        case when '' = campo49 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo49,'')) as char(20))  end   +''+

        case when '' = campo50 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo50,'')) as char(20))  end
    from #T_cabecera
end
else
begin
    select @F_cabecera =
        'NRO.'   +'|'+
        campo1   +'|'+
        campo2   +'|'+
        'COD_SOLICITUD'  +'|'+
        campo3   +'|'+
        campo4   +'|'+
        campo5   +'|'+
        campo6   +'|'+
        campo7   +'|'+
        campo8   +'|'+
        campo9   +'|'+

        campo10  +'|'+
        campo11  +'|'+
        campo12  +'|'+
        campo13  +'|'+
        campo14  +'|'+
        campo15  +'|'+
        campo16  +'|'+
        campo17  +'|'+
        campo18  +'|'+
        campo19  +'|'+

        campo20  +'|'+
        campo21  +'|'+
        campo22  +'|'+
        campo23  +'|'+
        campo24  +'|'+
        campo25  +'|'+
        campo26  +'|'+
        campo27  +'|'+
        campo28  +'|'+
        campo29  +'|'+

        campo30  +'|'+
        campo31  +'|'+
        campo32  +'|'+
        campo33  +'|'+
        campo34  +'|'+
        campo35  +'|'+
        campo36  +'|'+
        campo37  +'|'+
        campo38  +'|'+
        campo39  +'|'+

        campo40  +'|'+
        campo41  +'|'+
        campo42  +'|'+
        campo43  +'|'+
        campo44  +'|'+
        campo45  +'|'+
        campo46  +'|'+
        campo47  +'|'+
        campo48  +'|'+
        campo49  +'|'+

        campo50
    from #T_cabecera
end

----------------------------------
-- FORMATO Y TITULO DEL REPORTE --
----------------------------------
select nombre_reporte  = 'carga_archivo.r'

set @F_ancho_reporte = LEN(@F_cabecera)

select
    @F_lineas_cabecera = REPLICATE('-', @F_ancho_reporte),
    @F_titulo          = 'Listado de datos de la carga'

exec dbo.sp_titulos2
    @I_nombre_reporte    = 'carga_archivo.r',
    @I_Formato_simple    = 0,
    @I_Titulo1           = @F_titulo,
    @I_Titulo2           = '',
    @I_Encabezado        = '',
    @I_Encab_con_lineas  = 1,
    @I_lineas_cabecera   = @F_lineas_cabecera,
    @I_ancho_reporte     = @F_ancho_reporte,
    @I_texto_con_formato = 2
    ----------------------------------------------------------------

select linea_texto = ''
--ESTADO DE LA CARGA
union all
select linea_texto = 'ESTADO_CARGA: '  + b.nombre
from notmst_carga a
    inner join pamnot_estado b
        on a.estado = b.estado
            and b.indicador = 'A'
            and b.fecha_proceso_hasta = '01-01-2050'
where tipo_tabla = 4
    and a.nro_carga = @I_nro_carga
    and a.fecha_proceso = @I_fecha
    and a.indicador = 'A'
    and a.fecha_proceso_hasta = '01-01-2050'
union all
--FECHA Y USUARIO DE CARGA
select linea_texto =
    'FECHA_CARGA: ' + convert(char(10), x.fecha_proceso, 105) + CHAR(13) +
    'USUARIO_CARGA: ' + isnull(c.nombcorto, 'S/N')
from notmst_carga x
    inner join climov_usuario c
        on c.fecha_proceso_hasta = '01-01-2050'
            and c.indicador = 'A'
            and c.cliente = x.usuario
where x.nro_carga = @I_nro_carga --1945819
    and x.estado = 0 --Cargada
union all
--FECHA Y USUARIO DE VALIDACION
select linea_texto =
    'FECHA_VALIDA: '   + convert(char(10), x.fecha_proceso, 105) + CHAR(13) +
    'USUARIO_VALIDA: ' + isnull(c.nombcorto,'S/N')
from notmst_carga x
    inner join climov_usuario c
        on c.fecha_proceso_hasta = '01-01-2050'
            and c.indicador = 'A'
            and c.cliente = x.usuario
where x.nro_carga = @I_nro_carga --1945819
    and x.estado = 1 --Aceptada

--DATOS
select linea_texto = ''
union all select linea_texto = @F_lineas_cabecera + CHAR(13)  + CHAR(13)
union all select linea_texto = ltrim(@F_cabecera)
union all select linea_texto = @F_lineas_cabecera

--------------------------------------
-- CONTENIDO DE LA CARGA DE ARCHIVO --
--------------------------------------
if @I_ascii <> 1 --NO ASCII
begin
    select linea_texto = + space(1) +
        cast(rtrim(isnull(secuencia ,'')) as char(6))    +''+

        case when '' = campo1  then cast('' as char(16))  else cast(rtrim(isnull(+'|'+campo1 ,'')) as char(16)) end   +''+
        case when '' = campo2  then cast('' as char(14))  else cast(rtrim(isnull(+'|'+campo2 ,'')) as char(14)) end   +''+
        case when '' = codigo_solicitud  then cast('' as char(14))  else cast(rtrim(isnull(+'|'+ codigo_solicitud ,'')) as char(14)) end   +''+
        case when '' = campo3  then cast('' as char(10))  else cast(rtrim(isnull(+'|'+campo3 ,'')) as char(10)) end   +''+
        case when '' = campo4  then cast('' as char(12))  else cast(rtrim(isnull(+'|'+campo4 ,'')) as char(12)) end   +''+
        case when '' = campo5  then cast('' as char(40))  else cast(rtrim(isnull(+'|'+campo5 ,'')) as char(40)) end   +''+
        case when '' = campo6  then cast('' as char(5 ))  else cast(rtrim(isnull(+'|'+campo6 ,'')) as char(5 )) end   +''+
        case when '' = campo7  then cast('' as char(16))  else cast(rtrim(isnull(+'|'+campo7 ,'')) as char(16)) end   +''+
        case when '' = campo8  then cast('' as char(50))  else cast(rtrim(isnull(+'|'+campo8 ,'')) as char(50)) end   +''+
        case when '' = campo9  then cast('' as char(40))  else cast(rtrim(isnull(+'|'+campo9 ,'')) as char(40)) end   +''+

        case when '' = campo10 then cast('' as char(40))  else cast(rtrim(isnull(+'|'+campo10,'')) as char(40)) end   +''+
        case when '' = campo11 then cast('' as char(40))  else cast(rtrim(isnull(+'|'+campo11,'')) as char(40)) end   +''+
        case when '' = campo12 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo12,'')) as char(20)) end   +''+
        case when '' = campo13 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo13,'')) as char(20)) end   +''+
        case when '' = campo14 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo14,'')) as char(20)) end   +''+
        case when '' = campo15 then cast('' as char(50))  else cast(rtrim(isnull(+'|'+campo15,'')) as char(50)) end   +''+
        case when '' = campo16 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo16,'')) as char(20)) end   +''+
        case when '' = campo17 then cast('' as char(15))  else cast(rtrim(isnull(+'|'+campo17,'')) as char(15)) end   +''+
        case when '' = campo18 then cast('' as char(10))  else cast(rtrim(isnull(+'|'+campo18,'')) as char(10)) end   +''+
        case when '' = campo19 then cast('' as char(12))  else cast(rtrim(isnull(+'|'+campo19,'')) as char(12)) end   +''+

        case when '' = campo20 then cast('' as char(7 ))  else cast(rtrim(isnull(+'|'+campo20,'')) as char(7 )) end   +''+
        case when '' = campo21 then cast('' as char(12))  else cast(rtrim(isnull(+'|'+campo21,'')) as char(12)) end   +''+
        case when '' = campo22 then cast('' as char(12))  else cast(rtrim(isnull(+'|'+campo22,'')) as char(12)) end   +''+
        case when '' = campo23 then cast('' as char(16))  else cast(rtrim(isnull(+'|'+campo23,'')) as char(16)) end   +''+
        case when '' = campo24 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo24,'')) as char(20)) end   +''+
        case when '' = campo25 then cast('' as char(5 ))  else cast(rtrim(isnull(+'|'+campo25,'')) as char(5 )) end   +''+
        case when '' = campo26 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo26,'')) as char(20)) end   +''+
        case when '' = campo27 then cast('' as char(16))  else cast(rtrim(isnull(+'|'+campo27,'')) as char(16)) end   +''+
        case when '' = campo28 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo28,'')) as char(20)) end   +''+
        case when '' = campo29 then cast('' as char(16))  else cast(rtrim(isnull(+'|'+campo29,'')) as char(16)) end   +''+

        case when '' = campo30 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo30,'')) as char(20)) end   +''+
        case when '' = campo31 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo31,'')) as char(20)) end   +''+
        case when '' = campo32 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo32,'')) as char(20)) end   +''+
        case when '' = campo33 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo33,'')) as char(20)) end   +''+
        case when '' = campo34 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo34,'')) as char(20)) end   +''+
        case when '' = campo35 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo35,'')) as char(20)) end   +''+
        case when '' = campo36 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo36,'')) as char(20)) end   +''+
        case when '' = campo37 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo37,'')) as char(20)) end   +''+
        case when '' = campo38 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo38,'')) as char(20)) end   +''+
        case when '' = campo39 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo39,'')) as char(20)) end   +''+
        case when '' = campo40 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo40,'')) as char(20)) end   +''+

        case when '' = campo41 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo41,'')) as char(20)) end   +''+
        case when '' = campo42 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo42,'')) as char(20)) end   +''+
        case when '' = campo43 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo43,'')) as char(20)) end   +''+
        case when '' = campo44 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo44,'')) as char(20)) end   +''+
        case when '' = campo45 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo45,'')) as char(20)) end   +''+
        case when '' = campo46 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo46,'')) as char(20)) end   +''+
        case when '' = campo47 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo47,'')) as char(20)) end   +''+
        case when '' = campo48 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo48,'')) as char(20)) end   +''+
        case when '' = campo49 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo49,'')) as char(20)) end   +''+

        case when '' = campo50 then cast('' as char(20))  else cast(rtrim(isnull(+'|'+campo50,'')) as char(20)) end
    from #T_contenido
end
else
begin  --REPORTE ASCCI
    select linea_texto = + space(1) +
        cast(secuencia as varchar(12)) +'|'+
        codigo_solicitud +'|'+
        campo1    +'|'+
        campo2    +'|'+
        campo3    +'|'+
        campo4    +'|'+
        campo5    +'|'+
        campo6    +'|'+
        campo7    +'|'+
        campo8    +'|'+
        campo9    +'|'+

        campo10   +'|'+
        campo11   +'|'+
        campo12   +'|'+
        campo13   +'|'+
        campo14   +'|'+
        campo15   +'|'+
        campo16   +'|'+
        campo17   +'|'+
        campo18   +'|'+
        campo19   +'|'+

        campo20   +'|'+
        campo21   +'|'+
        campo22   +'|'+
        campo23   +'|'+
        campo24   +'|'+
        campo25   +'|'+
        campo26   +'|'+
        campo27   +'|'+
        campo28   +'|'+
        campo29   +'|'+

        campo30   +'|'+
        campo31   +'|'+
        campo32   +'|'+
        campo33   +'|'+
        campo34   +'|'+
        campo35   +'|'+
        campo36   +'|'+
        campo37   +'|'+
        campo38   +'|'+
        campo39   +'|'+
        campo40   +'|'+

        campo41   +'|'+
        campo42   +'|'+
        campo43   +'|'+
        campo44   +'|'+
        campo45   +'|'+
        campo46   +'|'+
        campo47   +'|'+
        campo48   +'|'+
        campo49   +'|'+

        campo50
    from #T_contenido
end

LINEA_salir_procedimiento:
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED

    if @@NESTLEVEL = 1
    begin
        set @F_obs_error = '@I_nro_carga: ' + isnull(cast(@F_obs_error as varchar(16)), 'Nulo')

        exec dbo.Grabar_log_GLB
            @I_usuario            = @F_usuario,
            @I_fecha_proceso      = @F_fecha_proceso,
            @I_observacion_error  = @F_obs_error,
            @I_nombre_sp          = 'proc_repnot_carga_archivo', --Nombre del procedimiento almacenado
            @I_fecha_desde        = @F_fecha_desde_log,
            @I_tipo_log           = 1,                           --1=reporte, 2=abm
            @I_es_error           = 0,                           --0=no es error, 1=si es error
            @I_error_msg          = ''
    end

    return 0
LINEA_error:
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED

    set @O_error_msg = 'proc_repnot_carga_archivo.§' + rtrim(@O_error_msg)

    if @@nestlevel = 1
    begin
        set @F_obs_error = '@I_nro_carga: ' + isnull(cast(@I_nro_carga as varchar(16)),'Nulo')

        exec dbo.Grabar_log_GLB
            @I_usuario            = @F_usuario,
            @I_fecha_proceso      = @F_fecha_proceso,
            @I_observacion_error  = @F_obs_error,
            @I_nombre_sp          = 'proc_repnot_carga_archivo', --Nombre del procedimiento almacenado
            @I_fecha_desde        = @F_fecha_desde_log,
            @I_tipo_log           = 1,                           --1=reporte, 2=abm
            @I_es_error           = 1,                           --0=no es error, 1=si es error
            @I_error_msg          = @O_error_msg
    end

    if @@nestlevel = 1
        raiserror(@O_error_msg, 16, -1)

    return -1
GO
