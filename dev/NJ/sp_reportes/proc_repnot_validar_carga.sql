﻿SET ANSI_NULLS, QUOTED_IDENTIFIER ON
GO
CREATE or alter procedure dbo.proc_repnot_validar_carga
    @O_error_msg    varchar(300) = '' output
WITH ENCRYPTION
as
/**************************************************************************************************/
/* DESCRIPCIÓN: Reporte para visualizar errores de validacion                                     */
/* OBSERVACIÓN: RD-8837 IV, identacion y mejoras en la descripcion de mensajes                    */
/* REV.CALIDAD: OK                                                                                */
/**************************************************************************************************/
set nocount on
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

declare
    @F_cabecera          varchar(1000)  = '',
    @F_ancho_reporte     smallint       = 0,
    @F_lineas_cabecera   varchar(1000)  = '',
    @F_titulo            varchar(1000)  = ''

-----------------------------------------------------------
-------         FORMATO Y TITULO DEL REPORTE      ---------
-----------------------------------------------------------
select nombre_reporte  = 'list_error_valid.r'

set @F_ancho_reporte = 150
set @F_lineas_cabecera = REPLICATE('-', @F_ancho_reporte)
set @F_titulo          = 'Listado de errores de validacion'

exec dbo.sp_titulos2
    @I_nombre_reporte    = 'list_error_valid.r',
    @I_Formato_simple    = 0,
    @I_Titulo1           = @F_titulo,
    @I_Titulo2           = '',
    @I_Encabezado        = '',
    @I_Encab_con_lineas  = 1,
    @I_lineas_cabecera   = @F_lineas_cabecera,
    @I_ancho_reporte     = @F_ancho_reporte,
    @I_texto_con_formato = 2

select linea_texto = @F_lineas_cabecera + CHAR(13) + CHAR(13)
union all select linea_texto = ltrim(@F_cabecera)
union all select linea_texto = @F_lineas_cabecera
----------------------------------
--        CONTENIDO             --
----------------------------------
declare
    @pam_tabla_errores TABLE (
        codigo      int,
        descripcion varchar(300)
    )

insert into @pam_tabla_errores (codigo, descripcion)
values
    (1 , 'EL VALOR NO ES ENTERO POSITIVO'),
    (2 , 'LOS VALORES DE LA FILA COMPLETA SE ENCUENTRA DUPLICADA EN EL DOCUMENTO'),
    (3 , 'LOS VALORES DE LA COLUMNA SON DIFERENTES A LA FILA: 1'),
    (6 , 'LA CIRCULAR YA SE ENCUENTRA REGISTRADA CON ESTADO ACTIVO'),
    (7 , 'VALOR INVÁLIDO, DEBE SER JURÍDICO O RELLENAR AP. PATERNO, AP. MATERNO, NOMBRE'),
    (8 , 'EL VALOR NO SE ENCUENTRA PARAMETRIZADO EN LA TABLA [pamnot_dato_clave]'),
    (9 , 'EL VALOR NO SE ENCUENTRA PARAMETRIZADO EN LA TABLA [pam_moneda] (BS., $US, UFV)'),
    (10, 'EL VALOR NO SE ENCUENTRA PARAMETRIZADO EN LA TABLA [pamnot_ente]'),
    (11, 'EL VALOR NO SE ENCUENTRA PARAMETRIZADO EN LA TABLA [pam_identificacion]'),
    (16, 'EL VALOR DEBE SER (1 = RETENCIÓN o 2 = LIBERACIÓN) PARA INDICAR EL TIPO DE NOTIFICACION '),
    (17, 'EL VALOR NO CUMPLE CON EL FORMATO DD/MM/AA'),
    (18, 'EL VALOR NO DEBE ESTAR VACIO'),
    (19, 'DEBE SER JURIDICO O PERSONA NATURAL'),
    (20, 'NO PUEDE TENER EXTENCION SI ES JURIDICO'),
    (21, 'VALORES INVÁLIDOS, COMPLETAR O ELIMINAR EL TIPO_DE_DATO_2 Y/O DATOS_CLAVE_2'),
    (22, 'VALORES INVÁLIDOS, COMPLETAR O ELIMINAR EL TIPO_DE_DATO_3 Y/O DATOS_CLAVE_3'),
    (23, 'EL VALOR NO CORRESPONDE A UN TIPO DE PERSONA JURIDICA')

select linea_texto = space(1)
    + 'FILA: '        + cast(b.fila + 1      as char(10 ))
    + 'COLUMNA: '     + cast(c.nombre_titulo as char(25 ))
    + 'DESCRIPCION: ' + cast(a.descripcion   as char(300))
from @pam_tabla_errores a
    inner join #T_error b
        on a.codigo = b.cod_error
    inner join pam_tipoTablaDetalle c
        on b.nro_campo = c.secuencia
where b.cod_error <> 0
    and c.idTabla = 97
    and c.indicador = 'A'
    and c.fecha_proceso_hasta = '01-01-2050'
order by b.fila, a.codigo

select linea_texto = space(1)
    + 'FILA: '        + cast(b.nro_linea   as char(10))
    + 'DESCRIPCION: ' + cast(a.descripcion as char(300))
from @pam_tabla_errores a
    inner join #T_tabla_error b
        on a.codigo = b.cod_error
order by b.nro_linea, a.codigo

select linea_texto = char(13)

----------------------------------------------------------------
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

return 0

linea_error:
    set @O_error_msg = 'proc_repnot_validar_carga.§' + ISNULL(@O_error_msg, 'NULO')

    if @@nestlevel = 1
        RAISERROR(@O_error_msg, 16, -1)

    return -1
GO
